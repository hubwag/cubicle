# Cubicle v0.8beta
## Streaming preprocessing for persisent homology of large greyscale 2D and 3D images.
Copyright IST Austria, 2015-2018; University of Florida, 2021+

## Founder and project lead:
Hubert Wagner (hub.wag@gmail.com)

## Contributors:
Teresa Heiss, Georg Osang

## Overview
* Cubicle computes the peristent homology (persistence diagram) of images.
* Input is a grey scale image in dimension 2 or 3.
* The software accepts only 'raw' binary files, but we provide conversion tools as python scripts.
* The image is read in small chunks -- so it's possible to handle images that may not store in RAM! But if the image is both huge and complicated you may be out of luck... Working on it though.
* Each chunk is efficienctly preprocessed -- so the approach is significantly faster and uses much less memory than a naive approach.
* The resulting persistence diagram is the same as the diagram yielded by the reduced boundary matrix of the filtered cubical complex of the image (with voxels interpreted as the top-dimensional cubical cells).

## Technical details (mostly for people working with very large files, software implementers etc)
* The image is read by chunks, e.g. for a 1024^3 image a chunk of size, say, 16*1024^2 is read in memory.
* Each chunk is preprocessed and a smaller representation is produced; this is done in a fashion similar to discrete Morse preprocessing.
* For each chunk the boundary of this smaller representation is written to disk; its size is usually much smaller than the size of the boundary matrix of the chunk.
* In the last step the boundary matrix is constructed using an external memory algorithm; we stress this boundary matrix is usually much smaller than the boundary matrix of the cubical complex corresponding to the entire image.
* The external-memory processing does not incur a significant overhead on modern SSD disks; we use the STXXL library which is well-suited for this kind of tasks. If necessary STXXL can be configured to work in memory, in which case the overhead is similar to using STL algorithms.
* Currently, the reconstructed matrix is stored in memory and reduced using the PHAT library; the disk storage was mostly added to enable distributing the matrix to multiple machines and using DIPHA or a similar distributed matrix-redutction library.
* For very large and/or complicated/images the resulting boundary matrix may not fit in memory. 
* As a rule of thumb: processing an image of size 512^3 should take a couple hundreds MB of memory and a couple of minutes. YMMV though.

## Related paper

Paper "Slice, Simplify and Stitch: Topology-Preserving Simplification Scheme for Massive Voxel Data" describing the techniques used in the software (and a benchmark) was published at SoCG. Have a look for more details. And please cite it if you use Cubicle in your paper.

bibtex:
```
@inproceedings{wagner2023slice,
  title={Slice, Simplify and Stitch: Topology-Preserving Simplification Scheme for Massive Voxel Data},
  author={Wagner, Hubert},
  booktitle={39th International Symposium on Computational Geometry (SoCG 2023)},
  year={2023},
  organization={Schloss Dagstuhl-Leibniz-Zentrum f{\"u}r Informatik}
}
```

## State of the software
* This is a beta version - please let me know if you found any errors or usability problems.
* The software is thoroughly tested; additionally the algorithms were proven to be correct.
* We're planning improvements both in terms of efficiency and usability (e.g. different types of inputs).

## Installation 

The main program is a command-line utility. Compiling 64bit executables is encouraged; 32bit executables will not be able to handle large files.

### Linux/Unix

`git clone https://bitbucket.org/hubwag/cubicle.git`

`cd cubicle`

`mkdir build`

`cd build`

`cmake .. -DCMAKE_BUILD_TYPE=Release` creates the build files (usually a unix makefile)

`cmake  --build  . --config Release` compiles the software, the executable is placed in the current directory. 


### Windows (Visual Studio)
## Tested with VS 2015, 17, 19, 22. If you encounter problems with newer versions, please contact us

Make sure you have recent cmake and visual studio installed (it's also possible to use mingw)


`git clone https://bitbucket.org/hubwag/cubicle.git`

`cd cubicle`

`mkdir build`

`cd build`

`cmake .. -DCMAKE_GENERATOR_PLATFORM=x64` creates the build files for x64 platform

`cmake  --build  . --config Release` compiles the software, the executable is placed in directory `Release`.  
  
  
## Basic usage 
`./Cubicle -f neghip_64x64x64.raw -s "64 64 64"` 

Note the quotes surrounding the size! This can be done simpler, since the size is already encoded in the filename:

`./Cubicle -f neghip_64x64x64.raw`

## Output 

For the above command  the output is saved in `neghip_64x64x64.raw_pairs.txt` which is a text file containing the resulting persistence diagram formatted
as the list of tuples `dimension, birth_value, death_value`.

## Input Image Format
The format for the input image is a binary 'raw' file where the gray-scale values of the voxels are stored linearly (as in a C array). In other words, the values are stored as if the indices/coordinates of the voxels were sorted lexicographically. 
Note that this is not consistent with how sizes of 2D images are specified.
Each value may be encoded as a 8, 6 or 32 bit signed or unsigned integer. 

For technical reasons, 8-bit unsigned are supported by default. 

Currently, to change this behavour, please modify the `file_elem_type` type in file `includes/common.h` and recompile the software. For example 16 bit unsigned values 
are configured like this `using file_elem_type = uint16_t;` . Additionally `filtration_value_t` should be updates to a wider type, so that the value larger than the maximum of `file_elem_type` can be expressed.

For example for inputs of 16-bit unsigned intger type, you can use:

`using file_elem_type = uint16_t;
using function_value_t = uint32_t;`

(This will be made more accessible soon.)


## Python utility scripts

Python scripts for converting images and stacks of images to raw files are provided in folder `scripts`.

3D raw images can be visualized for example with ImageVis3D: [link to ImageVis3D](http://www.sci.utah.edu/software/imagevis3d.html)

## Usage options
`./Cubicle -f <path to the input image> -s "<size1> <size2> <size3>" [-w <width>]  [-c <num_chunks>] [-p <num_threads>]`
with
1. `-f <path to the input image>` (required)
  path to a three-dimensional gray scale image, as a binary file. (More information, see Section Input Image Format.)
  The peristence diagram  of this image will be computed.
  
2. `-s "<size1> <size2> <size3>"` (required if none of the other ways to specify the size were used (see Section Advanced Options))
  three integers specifying the size of the image
  (e.g., `-s "1000 500 2000"` for a 1000 x 500 x 2000 image)
  
3. `-w <width>` (optional, alternative to -s)
  an integer,
  `-w <width>` is equivalent to `-s "<width> <width> <width>"`
  (Do not use -w together with -s.)  
  
4. `-c <num_chunks>` (optional, default = )
  an integer specifying the number of chunks the image should be cut into. The image will be sliced along to the first axis. Use many chunks to save memory.
  
5. `-p <num_threads>` (optional, default = number of physical cores available)
  an integer specifying the number of worker threads used for the computations. By deftult all cores will be utilized, decrease the number if you need to use the computer for other things...


## 2D images
You can use the 3 dimensional version by specifying one of the dimensions to be 1:
`./Cubicle -f Planck_500x1000.raw -s "500 1000 1"` computes the persistence of the two-dimensional 500x1000 image. 
