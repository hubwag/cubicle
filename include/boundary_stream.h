#pragma once
#include <string>

#include <stxxl/sorter>
#include <stxxl/io>
#include <stxxl/sort>
#include <stxxl/vector>
#include <stxxl/stream>
#include <stxxl/map>

#include "phat/boundary_matrix.h"
#include "phat/compute_persistence_pairs.h"
#include "phat/algorithms/twist_reduction.h"

#include "stxxl_comparators.h"
#include "external_merge.h"
#include "common.h"

namespace fields {
  enum fields { dim, value, old_id, new_id };
};

std::vector<int> get_f_vector(std::vector<index_t> &cubes) {
  std::vector<int> dims(embedding_dim + 1, 0);
  for (auto c : cubes) dims[get_dim(c)]++;
  return dims;
}

template <typename coface_face_id_stream_t>
void emit_phat_matrix_columns(coface_face_id_stream_t &coface_face, phat::boundary_matrix<> &mat)
{
  phat::column col;
  size_t last_coface = -1;
  coface_face.push_back(std::make_pair(-1, -1)); // Put sentinel.
  for (auto x : coface_face)
  {
    if (x.first != last_coface && last_coface != -1) {
      // Emit what we gathered.
      std::sort(col.begin(), col.end());
      mat.set_col(last_coface, col);
      col.clear();
    }
    col.push_back(x.second);
    last_coface = x.first;
  }
  coface_face.pop_back(); // Pop sentinel.
}

template<typename dim_val_id_ind_stream_t>
void get_diagram_using_stdmap_BAD_MEMORY(dim_val_id_ind_stream_t & dim_val_id_ind, phat::persistence_pairs &pairs, persistence_diagram_t &prs)
{
  using std::get;
  std::unordered_map<cube_id_t, function_value_t> vals;
  std::unordered_map<cube_id_t, int> dims;

  for (auto &x : dim_val_id_ind) {
    const auto dim = get<fields::dim>(x);
    const auto val = get<fields::value>(x);
    const auto id = get<fields::new_id>(x);

    assert(vals.count(id) == 0);
    assert(dims.count(id) == 0);
    vals.insert(std::make_pair(id, val));
    dims.insert(std::make_pair(id, dim));
  }

  for (size_t i = 0; i < pairs.get_num_pairs(); i++) {
    auto p = pairs.get_pair(i);  

    assert(vals.count(p.first));
    assert(vals.count(p.second));
    assert(dims.count(p.first));

    const function_value_t bf = vals[p.first];
    const function_value_t df = vals[p.second];

    // int dim_old = get<fields::dim>(dim_val_id_ind[p.first]);  
    if (df - bf >= 1) {
      int dim = dims[p.first];
      prs.emplace_back(dim, bf, df);
    }
  }
}

template<typename dim_val_id_ind_stream_t, typename coface_face_id_stream_t>
void translate_ids(dim_val_id_ind_stream_t &dim_val_id_ind, coface_face_id_stream_t &coface_face)
{   
  using std::get;
  auto get_identity = [](const auto &x) { return x; };

  // stxxl::stats_data stats_before(*stxxl::stats::get_instance());

  Stopwatch st;

  stxxl::sort(dim_val_id_ind.begin(), dim_val_id_ind.end(),
    comparator_cube_descriptor<decltype(get_identity)>(
      get_identity), stxxl_sort_memory_in_bytes);

  std::cout << "done first sorting by dim/val " << st.lap() << std::endl;
  std::cout << "nr columns: " << dim_val_id_ind.size() << std::endl;

  for (size_t i = 0; i < dim_val_id_ind.size(); i++)
    get<fields::new_id>(dim_val_id_ind[i]) = i;
  std::cout << "done assigning new ids " << st.lap() << std::endl;

  using pair_t = typename coface_face_id_stream_t::value_type;

  // TODO: there may be some spurious sorts done inside. Can we avoid them?
  join_and_update_ext_generic<comparator_cube_descriptor, my_comparator_pair>(
    dim_val_id_ind, coface_face,
    [](const cube_descriptor &a) -> const cube_id_t &{ return get<fields::old_id>(a); },
    [](const cube_descriptor &a) { return get<fields::new_id>(a); },
    [](const pair_t &a) -> const cube_id_t&{ return a.second; },
    [](pair_t &a) -> cube_id_t&{ return a.second; });
  std::cout << "after first join " << st.lap() << std::endl;

  join_and_update_ext_generic<comparator_cube_descriptor, my_comparator_pair>(
    dim_val_id_ind, coface_face,
    [](const cube_descriptor &a)
      -> const cube_id_t&{ return get<fields::old_id>(a); },
    [](const cube_descriptor &a) { return get<fields::new_id>(a); },
    [](const pair_t &a) -> const cube_id_t&{ return a.first; },
    [](pair_t &a) -> cube_id_t&{ return a.first; });
  std::cout << "after second join " << st.lap() << std::endl;

  stxxl::sort(coface_face.begin(), coface_face.end(),
    my_comparator_pair<decltype(get_identity)>{
    get_identity}, stxxl_sort_memory_in_bytes);

  auto get_new_id = [](const auto &x) { return get<fields::new_id>(x); };
  stxxl::sort(dim_val_id_ind.begin(), dim_val_id_ind.end(),
    comparator_cube_descriptor<decltype(get_new_id)>(
      get_new_id), stxxl_sort_memory_in_bytes);

  std::cout << "sorted again " << st.lap() << std::endl;

  // stxxl::stats_data stats_after(*stxxl::stats::get_instance());
  // std::cout << "STXXL STATS IN MERGE: " << stats_after - stats_before << std::endl;  
}

template<typename dim_val_id_ind_stream_t>
void translate_diagram_to_values(phat::persistence_pairs &pairs, dim_val_id_ind_stream_t &dim_val_id_ind, persistence_diagram_t &prs)
{
  Stopwatch st;
  using std::get;

  // b_ind, d_ind, dim, b_val, d_val
  using pers_tuple = std::tuple<cube_id_t, cube_id_t, int, function_value_t, function_value_t>;
  using pers_vector_t = stxxl::VECTOR_GENERATOR<pers_tuple, stxxl_config::BlocksPerPage, stxxl_config::PagesInCache, stxxl_config::BlockSizeInBytes>::result;
  pers_vector_t pers_tuple_vector;
  pers_tuple_vector.reserve(pairs.get_num_pairs());

  for (size_t i = 0; i < pairs.get_num_pairs(); i++) {
    const auto b_id = pairs.get_pair(i).first;
    const auto d_id = pairs.get_pair(i).second;
    pers_tuple_vector.push_back(std::make_tuple(b_id, d_id, -1, -1, -1));
  }

  // update b_val
  join_and_update_ext_generic<comparator_cube_descriptor, comparator_pers_tuple>(
    dim_val_id_ind, pers_tuple_vector,
    [](const cube_descriptor &a) -> const cube_id_t &{ return get<fields::new_id>(a); },
    [](const cube_descriptor &a) { return get<fields::value>(a); },
    [](const pers_tuple &a) { return get<0>(a); },
    [](pers_tuple &a) -> function_value_t& { return get<3>(a); });
  std::cout << "after join/b_val " << st.lap() << std::endl;

  // update dim (of birth cube)
  join_and_update_ext_generic<comparator_cube_descriptor, comparator_pers_tuple>(
    dim_val_id_ind, pers_tuple_vector,
    [](const cube_descriptor &a) -> const cube_id_t &{ return get<fields::new_id>(a); },
    [](const cube_descriptor &a) { return get<fields::dim>(a); },
    [](const pers_tuple &a) { return get<0>(a); },
    [](pers_tuple &a) -> int& { return get<2>(a); });
  std::cout << "after join/dim " << st.lap() << std::endl;

  // update d_val
  join_and_update_ext_generic<comparator_cube_descriptor, comparator_pers_tuple>(
    dim_val_id_ind, pers_tuple_vector,
    [](const cube_descriptor &a) -> const cube_id_t &{ return get<fields::new_id>(a); },
    [](const cube_descriptor &a) { return get<fields::value>(a); },
    [](const pers_tuple &a) { return get<1>(a); },
    [](pers_tuple &a) -> function_value_t& { return get<4>(a); });
  std::cout << "after join/d_val " << st.lap() << std::endl;

  for (auto x : pers_tuple_vector) {
    const function_value_t bf = get<3>(x);
    const function_value_t df = get<4>(x);

    assert(df>=0);
    assert(df>=0);

    if (df - bf >= 1) {
      const int dim = get<2>(x);
      assert(dim>=0);
      prs.emplace_back(dim, bf, df);
    }
  }
}

void output_persistence(persistence_diagram_t prs, std::ofstream &out)
{
  using std::get;
  for (const auto p : prs)
    out << get<0>(p) << ' ' << get<1>(p) << ' ' << get<2>(p) << '\n';  
}

template <typename dim_val_id_ind_stream_t, typename coface_face_id_stream_t>
persistence_diagram_t get_sorted_matrix_from_streams(const std::string &filename,
  dim_val_id_ind_stream_t &dim_val_id_ind,
  coface_face_id_stream_t &coface_face) {
  using std::get;
  auto get_identity = [](const auto &x) { return x; };  

  Stopwatch st;
  translate_ids(dim_val_id_ind, coface_face);

  std::cout << "translated ids in " << st.lap() << std::endl;

  phat::persistence_pairs pairs;
  {
    auto n = dim_val_id_ind.size();
    phat::boundary_matrix<> mat;
    mat.set_num_cols(n);

    for (auto cube : dim_val_id_ind) {
      const auto dim = get<fields::dim>(cube);
      const auto col_nr = get<fields::new_id>(cube);
      mat.set_dim(col_nr, dim);
    }

    std::cout << "set dimensions in phat matrix in " << st.lap() << std::endl;
    std::cout << "start constructing phat matrix " << std::endl;

    emit_phat_matrix_columns(coface_face, mat);
     
    std::cout << "constructed matrix " << st.lap() << std::endl;

    // mat.save_binary(filename + "_mat.bin");
    // std::cout << "dumped to disk (bin) " << st.lap() << std::endl;    
    // mat.save_ascii("tmp_mat.txt");

    std::cout << "reducing matrix with PHAT" << std::endl;
    phat::compute_persistence_pairs<phat::twist_reduction>(pairs, mat);
    std::cout << "REDUCED matrix " << st.lap() << std::endl;
  }

  std::ofstream out(filename + "_pairs.txt");  
  
  persistence_diagram_t prs;
  translate_diagram_to_values(pairs, dim_val_id_ind, prs);
  std::cout << "TRANSLATED PERS PAIRS TO VALUE in " << st.lap() << std::endl;

  // persistence_diagram_t prs; 
  // get_diagram_using_stdmap_BAD_MEMORY(dim_val_id_ind, pairs, prs);    
  // std::sort(prs.begin(), prs.end());

  std::sort(prs.begin(), prs.end());

  std::cout << "SORTED PERS PAIRS BY VALUE in " << st.lap() << std::endl;

  output_persistence(prs, out);
  std::cout << "saved sorted pairs " << st.lap() << std::endl;

  return prs;
}

class concurrent_boundary_stream {  
  using face_pair_t = std::pair < cube_id_t, cube_id_t >;  
  using external_vector_relations_t = stxxl::VECTOR_GENERATOR<face_pair_t, stxxl_config::BlocksPerPage, stxxl_config::PagesInCache, stxxl_config::BlockSizeInBytes>::result;
  using external_vector_cells_t = stxxl::VECTOR_GENERATOR<cube_descriptor, stxxl_config::BlocksPerPage, stxxl_config::PagesInCache, stxxl_config::BlockSizeInBytes>::result;

  std::vector<external_vector_relations_t> relations;
  std::vector<external_vector_cells_t> cells;

public:
  size_t get_nr_relations() const {
    size_t s = 0;
    for (const auto v : relations)
      s += v.size();
    return s;
  }

  concurrent_boundary_stream(size_t num_chunks = 0) {
    relations.resize(num_chunks);
    cells.resize(num_chunks);
    std::cout << "STXXL CACHE SHOULD USE ROUGHLY: " << 2 * BlocksPerPage * PagesInCache * BlockSizeInBytes / 1'000'000 << " Mb per slice" << std::endl;
    std::cout << "STXXL CACHE SHOULD USE ROUGHLY: " << num_chunks * 2 * BlocksPerPage * PagesInCache * BlockSizeInBytes / 1'000'000 << " Mb in total" << std::endl;
  }

  void put_cell(int dim, cube_id_t id, function_value_t fval, size_t chunk) {
    if (DEBUG) 
      std::cout << "\tnew cell: " << dim << " " << fval << " " << id << std::endl;
    cells[chunk].push_back(cube_descriptor{ dim, fval, id, 0, false });
  }

  void put_relation(cube_id_t face, cube_id_t coface, size_t chunk) {    
    // if (DEBUG) 
      // std::cout << "\t!!!NEW REL: " << coface << " " << face << std::endl;

    // Note the swap here -- we put coface, face !
    relations[chunk].push_back(std::make_pair(coface, face));
  }

  persistence_diagram_t compute_persistence(const std::string &filename) {
    using rel_vec = stxxl::VECTOR_GENERATOR<std::pair<cube_id_t, cube_id_t>, stxxl_config::BlocksPerPage, stxxl_config::PagesInCache, stxxl_config::BlockSizeInBytes>::result;
    using cells_vec = stxxl::VECTOR_GENERATOR<cube_descriptor, stxxl_config::BlocksPerPage, stxxl_config::PagesInCache, stxxl_config::BlockSizeInBytes>::result;

    rel_vec all_relations;
    cells_vec all_cells;

    // stxxl::stats_data stats_before(*stxxl::stats::get_instance());    

    catenate(this->relations, all_relations);
    catenate(this->cells, all_cells);   

    return get_sorted_matrix_from_streams(filename, all_cells, all_relations);
  }
};