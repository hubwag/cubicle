/*  Copyright 2015-2018 IST Austria
    File contributed by: Hubert Wagner.
    This file is part of Cubicle.
    Cubicle is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
    Cubicle is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.
    You should have received a copy of the GNU Lesser General Public License
    along with Cubicle.  If not, see <http://www.gnu.org/licenses/>. */
    
#pragma once

template <typename key_fun_t>
struct my_comparator_pair {
  using tp = cube_id_t;
  using pr = std::pair<cube_id_t, cube_id_t>;
  key_fun_t key;

  my_comparator_pair(key_fun_t &key) : key(key) {}
  
  bool operator()(const pr &a, const pr &b) const { return key(a) < key(b); }

  constexpr pr min_value() const {
    return std::make_pair(std::numeric_limits<size_t>::min(),
                          std::numeric_limits<size_t>::min());
  }

  constexpr pr max_value() const {
    return std::make_pair(std::numeric_limits<tp>::max(),
                          std::numeric_limits<tp>::max());
  }
};


template <typename key_fun_t>
struct comparator_cube_descriptor {  
  using pr = std::tuple<int, function_value_t, cube_id_t, cube_id_t, bool>;
  key_fun_t key;

  comparator_cube_descriptor(key_fun_t &key) : key(key) {}

  bool operator()(const pr &a, const pr &b) const { return key(a) < key(b); }

  constexpr pr min_value() const { return pr{-1, -1, -1, -1, false}; }

  constexpr pr max_value() const {   
    return pr{100, std::numeric_limits<function_value_t>::max(), std::numeric_limits<cube_id_t>::max(), std::numeric_limits<cube_id_t>::max(), false};  // first is dim
  }
};

template <typename key_fun_t>
struct comparator_pers_tuple {  
  using pr = std::tuple<cube_id_t, cube_id_t, int, function_value_t, function_value_t>;
  key_fun_t key;

  comparator_pers_tuple(key_fun_t &key) : key(key) {}

  bool operator()(const pr &a, const pr &b) const { return key(a) < key(b); }

  constexpr pr min_value() const { return pr{ -100, -100, -100, -100, -100 }; }

  constexpr pr max_value() const {
    return pr{
      std::numeric_limits<cube_id_t>::max(),
      std::numeric_limits<cube_id_t>::max(), 
      std::numeric_limits<int>::max(), 
      std::numeric_limits<function_value_t>::max(),
      std::numeric_limits<function_value_t>::max() };     
  }
};

template<typename t, typename k>
struct keyed_comparator {
  using key_fun_t = k;
  using pr = std::tuple<cube_id_t, cube_id_t, int, function_value_t, function_value_t>;
  key_fun_t key;

  keyed_comparator(key_fun_t &key) : key(key) {}

  bool operator()(const pr &a, const pr &b) const { return key(a) < key(b); }

  constexpr pr min_value() const { return pr{ -100, -100, -100, -100, -100 }; }

  constexpr pr max_value() const {
    return pr{
      std::numeric_limits<cube_id_t>::max(),
      std::numeric_limits<cube_id_t>::max(),
      std::numeric_limits<int>::max(),
      std::numeric_limits<function_value_t>::max(),
      std::numeric_limits<function_value_t>::max() };
  }
};

/*
template<typename key_t>
struct keyed_comparator<typename comparator_pers_tuple<key_t>::pr, key_t> {
  using key_fun_t = key_t;
  using pr = std::tuple<cube_id_t, cube_id_t, int, function_value_t, function_value_t>;
  key_fun_t key;

  keyed_comparator(key_fun_t &key) : key(key) {}

  bool operator()(const pr &a, const pr &b) const { return key(a) < key(b); }

  constexpr pr min_value() const { return pr{ -100, -100, -100, -100, -100 }; }

  constexpr pr max_value() const {
    return pr{
      std::numeric_limits<cube_id_t>::max(),
      std::numeric_limits<cube_id_t>::max(),
      std::numeric_limits<int>::max(),
      std::numeric_limits<function_value_t>::max(),
      std::numeric_limits<function_value_t>::max() };
  }
};*/

template<typename key_t>
struct keyed_comparator<typename comparator_cube_descriptor<key_t>::pr, key_t> : public comparator_cube_descriptor<key_t> {
  keyed_comparator(key_t &k) : comparator_cube_descriptor<key_t>(k) {}
};

template<typename key_t>
struct keyed_comparator<typename my_comparator_pair<key_t>::pr, key_t> : my_comparator_pair<key_t> {
  keyed_comparator(key_t &k) : my_comparator_pair<key_t>(k) {}
};