/*  Copyright 2015-2018 IST Austria
    File contributed by: Hubert Wagner.
    This file is part of Cubicle.
    Cubicle is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
    Cubicle is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.
    You should have received a copy of the GNU Lesser General Public License
    along with Cubicle.  If not, see <http://www.gnu.org/licenses/>. */
#pragma once
#include <bitset>
#include <future>
#include <map>
#include <limits>
#include <string> 
#include <sstream>
#include <regex>
#include <unordered_map>
#include <unordered_set>

#include <iomanip> 
#include <locale>

#include <blitz/array.h>
#include <blitz/blitz.h>
#include <blitz/tinyvec-et.h>

#include "container_io.h"

const size_t embedding_dim = 3;
using cube_id_t = int64_t;

const int overlap = 2; // How many slices of voxels overlap between adjacent images.

//using file_elem_type = uint8_t;
using file_elem_type = uint8_t;
using function_value_t = int64_t;
using matrix_t = blitz::Array<function_value_t, embedding_dim>;
using index_t = blitz::TinyVector<int32_t, embedding_dim>;

const function_value_t INFINITE_FUNCTION_VALUE =
    std::numeric_limits<function_value_t>::max();

using euler_pair_t = std::pair<function_value_t, int>;
using euler_map_t = std::unordered_map<function_value_t, int>;

using persistence_diagram_t =
    std::vector<std::tuple<int, function_value_t, function_value_t>>;

const bool DEBUG = false;

#include "bittree.h"
// more using's below!

#include "blitz_operators.h"
#include "cube_boundary_relations.h"


struct statistics {
  size_t nr_columns_final = -1;
  size_t nr_ones_final = -1;
  float time_in_chunks = -1;
  float time_in_postproc = -1;
  float time_in_reduction = -1;
};


struct voxel_traits {
  static index_t to_big_coords(const index_t &small_coords) {
    return 2 * small_coords + 1;  // !!!
  }

  static index_t to_small_coords(const index_t &big_coords) {
    return (big_coords - 1) / 2;
  }

  static index_t enlarge_extent(const index_t &small_extent) {
    return small_extent * 2 + 1;
  }

  template <typename val_t, typename other_val_t>
  static bool is_this_my_face(const val_t &my_val, const other_val_t &his_val) {
    return my_val < his_val;
  }

  enum : size_t { VOXEL_TYPE_ID = (1ULL << embedding_dim) - 1 };

  static const index_t get_base() { return index_t(1); }
};

size_t num_voxels(index_t sz) {
  size_t p = 1;
  for (auto x : sz) p *= x;
  return p;
}

struct vertex_traits {
  index_t to_big_coords(const index_t &small_coords) {
    return 2 * small_coords;  // !!!
  }

  static index_t to_small_coords(const index_t &big_coords) {
    return big_coords / 2;
  }

  static index_t enlarge_extent(const index_t &small_extent) {
    return small_extent * 2 - 1;
  }

  template <typename val_t, typename other_val_t>
  static bool is_this_my_face(const val_t &my_val, const other_val_t &his_val) {
    return my_val > his_val;
  }

  static const index_t get_base() { return index_t(0); }
};

// It agrees with blitz iterator ordering
cube_id_t get_id_with_extent(const index_t &extent, const index_t &coords) {
  cube_id_t id = 0;
  size_t slice = 1;
  for (int i = coords.length() - 1; i >= 0; i--) {
    id += slice * coords[i];
    slice *= extent[i];
  }
  return id;
}

index_t get_index_with_extent(const index_t &extent, cube_id_t id) {
  index_t ind;

  for (int i = extent.length() - 1; i >= 0; i--) {
    ind[i] = id % extent[i];
    id /= extent[i];
  }
  return ind;
}

cube_id_t get_cube_type(index_t ind) {
  return get_id_with_extent(index_t{ 2 }, index_t(ind % 2));
}

struct less_mask_class {
  template <typename mask_t>
  bool operator()(const mask_t &a, const mask_t &b) const {
    for (size_t i = 0; i < a.size(); i++) {
      if (a[i] < b[i]) return true;
      if (a[i] > b[i]) return false;
    }
    return false;
  }
};

constexpr int64_t ipow(int64_t base, int exp, int64_t result = 1) {
  return exp < 1 ? result : ipow(base * base, exp / 2,
    (exp % 2) ? result * base : result);
}

template <int D>
struct neighbour_traits {
  using mask_t = std::bitset<ipow(3, D)>;
  using less_mask = less_mask_class;
};

template <>
struct neighbour_traits<2> {
  using mask_t = size_t;
  using less_mask = std::less<size_t>;
};

template <>
struct neighbour_traits<1> {
  using mask_t = size_t;
  using less_mask = std::less<size_t>;
};

// dim, val, id, [newid placeholder], is_at_interface
using cube_descriptor =
std::tuple<int8_t, function_value_t, cube_id_t, cube_id_t, bool>;

// plain int version is 2x faster than using bitset...
template <>
struct neighbour_traits<3> {
  using mask_t = size_t;
  using less_mask = std::less<size_t>;
};

using face_number_t = char;  // up to dim 5?
using mask_t = neighbour_traits<embedding_dim>::mask_t;
using less_mask_t = neighbour_traits<embedding_dim>::less_mask;

template<class T>
std::string sep(T value)
{
  std::stringstream ss;
  ss.imbue(std::locale(""));
  ss << std::fixed << value;
  return ss.str();
}

void test_get_id() {
  index_t sz{ 5 };
  matrix_t a(sz);

  int i = 0;
  for (auto &x : a) x = i++;

  for (auto it = a.begin(); it != a.end(); ++it)
    assert(*it == get_id_with_extent(a.extent(), it.position()));
}

template <typename counted_type, int bucket_size = 1>
struct histogram {
  std::unordered_map<counted_type, size_t> h;
  size_t all = 0;

  void add(const counted_type &observation, size_t times = 1) {
    h[observation / bucket_size * bucket_size] += times;
    all += times;
  }

  void report() {
    std::cout << "hist: " << h << std::endl;
    std::vector<std::pair<size_t, counted_type>> v;
    for (auto x : h) v.emplace_back(std::make_pair(x.second, x.first));
    // I want python now...
    std::sort(v.begin(), v.end());
    std::cout << "sorted (times : val):" << v << std::endl;
  }

  void report_percents() {
    std::cout << "hist: " << h << std::endl;
    std::vector<std::pair<double, counted_type>> v;
    for (auto x : h)
      v.emplace_back(std::make_pair(100.0 * x.second / all, x.first));
    // I want python now...
    std::sort(v.begin(), v.end());
    std::cout << "sorted (percent : val):" << v << std::endl;
  }
};

index_t parse_size(std::string s)
{
  std::replace(s.begin(), s.end(), 'x', ' ');
  std::stringstream ss(s);
  index_t ind{ 0 };
  for (int i = 0; i < ind.length() && ss.good(); i++)
    ss >> ind[i];
  return ind;
}

struct Stopwatch {
  std::chrono::time_point<std::chrono::high_resolution_clock> last;
  Stopwatch() {
    last = std::chrono::high_resolution_clock::now();
  }

  double lap() {
    auto now = std::chrono::high_resolution_clock::now();
    auto duration = std::chrono::duration_cast<std::chrono::duration<double>>(
      now - last).count();
    last = now;
    return duration;
  }
};
