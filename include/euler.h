/*  Copyright 2015-2018 IST Austria
    File contributed by: Teresa Heiss.
    This file is part of Cubicle.
    Cubicle is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
    Cubicle is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.
    You should have received a copy of the GNU Lesser General Public License
    along with Cubicle.  If not, see <http://www.gnu.org/licenses/>. */
    
#pragma once 

#include "common.h"
#include "cubical_complex.h"

std::vector<euler_pair_t> from_map_of_changes_to_euler(
  const euler_map_t &euler_changes_map) {

  //Converting the unordered_map into a vector
  // and leaving out the pairs that indicate no change in the euler char.
  std::vector<euler_pair_t> euler;
  for (euler_pair_t function_value_and_euler : euler_changes_map) {
    if (function_value_and_euler.second != 0) {
      euler.push_back(function_value_and_euler);
    }
  }

  //Sorting (lexicographically)
  std::sort(euler.begin(), euler.end());

  //Accummulating over the vector of changes to get the euler characteristic
  for (size_t i = 1; i < euler.size(); i++) {
    euler[i].second += euler[i - 1].second;
  }

  //The second element of the last entry in euler should be 1,
  // because this is the euler characteristic of a completely filled rectangle.
  assert(euler.back().second == 1);

  return euler;
}

class euler_merger_sparse {
protected:
  std::vector<euler_map_t> euler_changes;
  size_t num_chunks;

public:
  euler_merger_sparse(size_t number_of_chunks = 1)
    : num_chunks(number_of_chunks),
    euler_changes(number_of_chunks) {
  }

  void update(function_value_t function_value, int change, size_t slice_index) {
    assert(slice_index >= 0);
    assert(slice_index < euler_changes.size());
    euler_changes[slice_index][function_value] += change;
  }

  std::vector<euler_pair_t> get_merged_euler_characteristic() {
    //Summing num_chunks many unordered_maps up to one unordered_map
    euler_map_t euler_changes_overall;
    for (size_t slice = 0; slice < num_chunks; slice++) {
      for (auto function_value_and_change : euler_changes[slice]) {
        function_value_t function_value = function_value_and_change.first;
        int change = function_value_and_change.second;
        euler_changes_overall[function_value] += change;
      }
    }

    std::vector<euler_pair_t> euler =
      from_map_of_changes_to_euler(euler_changes_overall);

    return euler;
  }
};

class euler_merger {
protected:
  std::vector<std::vector<int>> euler_changes;
  size_t num_chunks;
  function_value_t min_val;
  function_value_t max_val;

public:
  euler_merger(size_t number_of_chunks = 1,
    function_value_t maximum_value =
      static_cast<function_value_t>(std::numeric_limits<file_elem_type>::max()),
    function_value_t minimum_value = 0)
    : num_chunks(number_of_chunks),
    max_val(maximum_value),
    min_val(minimum_value),
    euler_changes(number_of_chunks) {
    assert(minimum_value <= maximum_value);
    for (size_t slice = 0; slice < number_of_chunks; slice++) {
      euler_changes[slice].resize(maximum_value - minimum_value + 1);
    }
  }

  void update(function_value_t value, int change, size_t slice_index) {
    assert(slice_index >= 0);
    assert(slice_index < euler_changes.size());
    assert(value - min_val >= 0);
    size_t index = value - min_val;
    assert(index < euler_changes[slice_index].size());

    euler_changes[slice_index][index] += change;
  }

  std::vector<euler_pair_t> get_merged_euler_characteristic() {

    std::vector<int> euler_whole(max_val - min_val + 1, 0);
    std::vector<euler_pair_t> euler_sparse;

    //Summing num_chunks many vectors up to one vector
    for (auto eul_changes_of_slice : euler_changes) {
      assert(euler_whole.size() == eul_changes_of_slice.size());
      for (size_t j = 0; j < euler_whole.size(); j++) {
        euler_whole[j] += eul_changes_of_slice[j];
      }
    }

    //Making the vector 'sparse'
    // i.e.: Collecting all the entries where the euler characteristic changes
    if (euler_whole[0] != 0) {
      euler_sparse.push_back(euler_pair_t(min_val, euler_whole[0]));
    }
    for (size_t i = 1; i < euler_whole.size(); i++) {
      euler_whole[i] += euler_whole[i - 1];
      if (euler_whole[i] != euler_whole[i - 1]) {
        euler_sparse.push_back(
          euler_pair_t(min_val + static_cast<function_value_t>(i), euler_whole[i])
        );
      }
    }

    //The last entry in euler should be 1,
    // because this is the euler characteristic of a completely filled rectangle.
    assert(euler_whole.back() == 1);
    assert(euler_sparse.back().second == 1);

    return euler_sparse;
  }
};

template<typename euler_merger_t>
void compute_euler_of_slice_with_merger(matrix_t &m,
  euler_merger_t &eul_merger,
  star_manager *stars_ptr,
  size_t current_slice = 0) {

  std::cout << "START computing Euler of slice " << current_slice
    << " with merger" << std::endl;
  Stopwatch sw;
  cubical_complex complex(m);

  complex.for_each_voxel([&](const cube_handle &x) {
    x.for_each_face_in_star([&](const cube_handle &c) {
      if (c.dim % 2 == 1) { //odd
        eul_merger.update(x.val, -1, current_slice);
      }
      else { //even
        eul_merger.update(x.val, +1, current_slice);
      }
    });
  });

  std::cout << "Computed Euler of slice " << current_slice
    << " with merger in " << sw.lap() << std::endl;
}

std::vector<euler_pair_t> from_persistence_to_euler_sparse(
  const persistence_diagram_t &persistence,
  function_value_t real_minimum_value) {

  euler_map_t euler_map;

  //The one connected component that never dies increases the euler char.
  euler_map[real_minimum_value]++;

  for (auto const &triple : persistence) {
    int dim = std::get<0>(triple);
    function_value_t birth = std::get<1>(triple);
    function_value_t death = std::get<2>(triple);

    if (dim % 2 == 1) { //odd
      euler_map[birth]--;
      euler_map[death]++;
    }
    else { //even
      euler_map[birth]++;
      euler_map[death]--;
    }
  }

  std::vector<euler_pair_t> euler = from_map_of_changes_to_euler(euler_map);
  return euler;
}