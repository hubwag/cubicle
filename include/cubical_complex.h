/*  Copyright 2015-2018 IST Austria
    File contributed by: Hubert Wagner.
    This file is part of Cubicle.
    Cubicle is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
    Cubicle is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.
    You should have received a copy of the GNU Lesser General Public License
    along with Cubicle.  If not, see <http://www.gnu.org/licenses/>. */
    
#pragma once

#include "common.h"

class cubical_complex;
class star_manager;

struct cube_handle {
  cube_id_t id;
  index_t big_ind;  // we don't always need that...
  function_value_t val;
  int type;  // can get from big_ind
  int dim;
  // int nr_in_voxel;

  /*
  cube_handle(cube_id_t id, const index_t &big_ind, function_value_t val = -1) :
    id(id), big_ind(big_ind), val(val) {
  }
  */

  cubical_complex *comp;

  bool operator<(const cube_handle &other) const {
    return id < other.id;
  }

  //cube_handle(const cube_handle &) = delete;
  //cube_handle &operator=(const cube_handle &) = delete;

  template <typename fun_t>
  void for_each_face_in_star(fun_t f) const;

  template <typename fun_t>
  void for_each_face(fun_t f) const;

  template <typename fun_t>
  void for_each_boundary(fun_t f) const;
};


class cubical_complex : public voxel_traits {
public:
  matrix_t &m;
  void init_faces();
  void init_bounaries();

  // same indexing
  std::vector<index_t> face_index_deltas;
  std::vector<ptrdiff_t>
    neighbour_address_offsest;  // address offsets to neighbours
  std::vector<cube_id_t> face_id_deltas;

  std::unordered_map<cube_id_t, face_number_t> delta_to_face_number;

  std::vector<std::vector<index_t>> type_to_voxel_index_offset;
  std::vector<std::vector<ptrdiff_t>> type_to_voxel_address_offset;

  std::vector<int> face_dims;
  std::vector<int> face_types;

  std::vector<mask_t> face_masks;
  std::vector<mask_t> coface_masks;

  std::vector<std::vector<index_t>> boundary_deltas;
  std::vector<std::vector<int>> boundary_id_deltas;

  const index_t large_extent;

  cube_id_t first_upper_interface_id;
  cube_id_t last_upper_interface_id;

  cube_id_t first_lower_interface_id;
  cube_id_t last_lower_interface_id;

  void init_interface_info() {
    size_t index0_first_upper = 2 + 2 * overlap;
    size_t index0_last_lower = this->large_extent(0) - 1 - 2 - 2 * overlap;

    index_t upper_index = { 0 };
    upper_index[0] = index0_first_upper;
    this->first_upper_interface_id = this->get_id(upper_index); 
    upper_index[0]++;
    this->last_upper_interface_id = this->get_id(upper_index) - 1;

    index_t collar_check_upper = index_t(1);
    collar_check_upper[0] = overlap ;
    if (m(collar_check_upper) == INFINITE_FUNCTION_VALUE) // we're first
    {
      this->first_upper_interface_id = -1;      
      this->last_upper_interface_id = -1;

      std::cout << "seems we're the first chunk!" << std::endl;
    }

    index_t lower_index = this->large_extent - 1;
    lower_index[0] = index0_last_lower;
    this->last_lower_interface_id = this->get_id(lower_index);
    lower_index[0]--;
    this->first_lower_interface_id = +1 + this->get_id(lower_index);    

    index_t collar_check_lower = index_t(1);
    collar_check_lower[0] = this->m.extent(0) - 1 - overlap;
    if (m(collar_check_lower) == INFINITE_FUNCTION_VALUE) // we're first
    {
      this->first_lower_interface_id = -1;
      this->last_lower_interface_id = -1;

      std::cout << "seems we're the last chunk!" << std::endl;
    }
  }

  bool is_inside_and_not_in_collar(const index_t &pos) {
    if (pos[0] <= overlap || pos[0] + overlap + 1 >= m.extent(0)) {
      return false;
    }

    for (size_t i = 1, n = pos.length(); i < n; ++i) {
      if (pos[i] <= 0 || pos[i] + 1 >= m.extent(i)) {
        return false;
      }
    }
    return true;
  }

public:
  cubical_complex(matrix_t &image)
    : m(image), large_extent(enlarge_extent(image.extent())) {
    init_faces();
    init_bounaries();
    init_interface_info();
  }

  bool is_interface_cell(cube_id_t cell_id) const {    
    //return (first_lower_interface_id <= cell_id && last_lower_interface_id >= cell_id) ||
    //  (first_upper_interface_id <= cell_id && last_upper_interface_id >= cell_id);
    auto index0 = this->get_index_from_id(cell_id)[0];
    return index0 == 6 || index0 == this->large_extent(0) - 7;
  }

  index_t get_voxel_small_index(const cube_id_t voxel_id) const {
    return ::get_index_with_extent(m.extent(), voxel_id);
  }

  cube_id_t get_voxel_small_id(const index_t big_ind) const {
    return ::get_id_with_extent(m.extent(), to_small_coords(big_ind));
  }

  cube_id_t get_voxel_small_id(cube_id_t big_id) const {
    return ::get_id_with_extent(m.extent(), to_small_coords(get_index_from_id(big_id)));
  }

  cube_id_t get_id(const cube_handle &cube) const {
    return ::get_id_with_extent(large_extent, cube.big_ind);
  }

  cube_id_t get_id(const index_t &cube_big_ind) const {
    return ::get_id_with_extent(large_extent, cube_big_ind);
  }

  index_t get_index_from_id(const cube_id_t id) const {
    return ::get_index_with_extent(large_extent, id);
  }

  size_t num_cells() const { return get_id(large_extent - 1) + 1; }

  size_t num_voxels() const { return m.numElements(); }

  size_t num_voxels_without_padding() const { 
    auto data_extent = this->m.extent();
    data_extent -= 2;
    data_extent[0] -= 2 * overlap;   

    return ::get_id_with_extent(data_extent, data_extent -1) + 1;
  }

  const function_value_t get_value_from_voxel_id(cube_id_t id) const {
    return m(get_voxel_small_index(id));
  }

  const function_value_t get_value_from_cube_id(cube_id_t id) const {
    const auto big_index = get_index_from_id(id);
    const index_t voxel_index = to_small_coords(big_index); // from traits
    return m(voxel_index);
  }

  function_value_t& get_value_from_cube_id(cube_id_t id) {
    const auto big_index = get_index_from_id(id);
    const index_t voxel_index = to_small_coords(big_index); // from traits
    return m(voxel_index);
  }

  template <typename fun_t>
  void for_each_face_in_star_of(const cube_handle &c, fun_t f) {
    auto signature = get_star_signature(c);
    // this call could be slow for higher dim with bitset...
    this->for_each_face_of(c, f, ~signature);
  }

  template <typename fun_t>
  void for_each_proper_face_of(const cube_handle &c, fun_t f,
    mask_t excluding = mask_t(0)) {

    const mask_t one = 1;
    cube_handle face_cube{};
    face_cube.comp = this;
    face_cube.dim = c.dim;

    for (size_t i = 0, n = face_id_deltas.size(); i < n; i++) {
      if ((excluding & (one << i)) ==
        (one << i))  // VERY SLOW for bitset!
        continue;

      const cube_id_t face_id = c.id + face_id_deltas[i];
      const int type_id = face_types[i];
      const int dim = face_dims[i];

      face_cube.type = type_id;
      face_cube.id = face_id;
      face_cube.dim = dim;
      // face_cube.nr_in_voxel = i;

      f(face_cube);
    }
  }

  template <typename fun_t>
  void for_each_face_of(const cube_handle &c, fun_t f,
    mask_t excluding = mask_t(0)) {
    const mask_t one = 1;
    cube_handle face_cube{};
    face_cube.comp = this;
    face_cube.dim = c.dim; // ?    

    for (size_t i = 0, n = face_id_deltas.size(); i < n; i++) {
      if ((excluding & (one << i)) ==
        (one << i))  // VERY SLOW sloow for bitset!
        continue;

      const cube_id_t face_id = c.id + face_id_deltas[i];
      const int type_id = face_types[i];
      const int dim = face_dims[i];

      face_cube.type = type_id;
      face_cube.id = face_id;
      face_cube.dim = dim;

      // face_cube.nr_in_voxel = i;

      f(face_cube);
    }
  }

  template <typename fun_t>
  void for_each_voxel(fun_t f) {
    for (auto it = m.begin(), end = m.end(); it != end; ++it) {      
      if (*it >= INFINITE_FUNCTION_VALUE)
        continue;

      const index_t ind = it.position();

      if (!is_inside_and_not_in_collar(ind)) {
        continue;
      }

      const index_t big = to_big_coords(ind);
      const cube_id_t id = get_id(big);
      const function_value_t val = *it;

      cube_handle c{ id, big, val };
      c.comp = this;
      c.dim = embedding_dim;
      c.type = VOXEL_TYPE_ID;
      f(c);
    }
  }

  template <typename fun_t>
  void for_each_voxel_including_interface(fun_t f) {
    for (auto it = m.begin(), end = m.end(); it != end; ++it) {
      if (*it >= INFINITE_FUNCTION_VALUE)
        continue;

      const index_t ind = it.position();

      const index_t big = to_big_coords(ind);
      const cube_id_t id = get_id(big);
      const function_value_t val = *it;

      cube_handle c{ id, big, val };
      c.comp = this;
      c.dim = embedding_dim;
      c.type = VOXEL_TYPE_ID;
      f(c);
    }
  }

  //
  const std::vector<index_t> &get_containing_voxels_offsets(
    const index_t &ind) const {
    const int type = get_cube_type(ind);
    return type_to_voxel_index_offset[type];
  }

  index_t get_voxel_with_star_containing_face(const cube_id_t id) {
    const index_t ind = get_index_from_id(id);
    const auto &big_indices_of_voxels = get_containing_voxels_offsets(ind);
    const auto mx_val = std::numeric_limits<function_value_t>::max();
    std::tuple<function_value_t, const function_value_t *, index_t> best(
      mx_val, 0, index_t{});
    for (const auto &off : big_indices_of_voxels) {
      const index_t global = off + ind;
      const index_t image_index = to_small_coords(global);
      const auto &val = m(image_index);

      best = std::min(best, std::make_tuple(val, &val, global));
    }
    return std::get<2>(best);
  }

  void enforce_subcomplex_property(mask_t &m) const {
    // this could be faster, we only need to access set bits...
    const mask_t voxel_mask = 1;
    for (size_t i = 0; i < neighbour_address_offsest.size(); i++) {
      if ((m & (mask_t(1) << i)) == (mask_t(1) << i))  // sloow for d > 3
      {
        const mask_t &sub_mask = face_masks[i] & ~voxel_mask;
        if ((m | sub_mask) != m)  // not all faces are present
          m ^= (mask_t(1) << i);  // remove the current cell, because it has missing (co)faces
      }
    }
  };

  void dump_star(const mask_t &m);  

  template <typename fun_t>
  void for_each_boundary_of(const cube_handle &c, fun_t f) {
    cube_handle bdry_cube = {};
    bdry_cube.dim = c.dim - 1;
    bdry_cube.comp = this;

    for (const int delta : boundary_id_deltas[c.type]) {
      const cube_id_t boundary_elem_id = c.id + delta;
      bdry_cube.id = boundary_elem_id;
      // bdry_cube.type = get_cube_type(get_index_from_id(bdry_cube.id));
      f(bdry_cube);
    }
  }

  template <typename a_t, typename b_t>
  bool is_my_face_with_tiebreak(const a_t &my_value, const b_t &his_value) const {
    return is_this_my_face(my_value, his_value) ||
      (my_value == his_value && &my_value < &his_value);
  }

  mask_t get_star_signature(const cube_handle &c) const {
    return get_star_signature(c.big_ind);
  }


  // Here we assume it actually comes from a cubical complex,
  // so the voxel needs to be present.
  mask_t get_star_signature(const index_t &big_ind) const {
    mask_t signature = 0;
    const mask_t one = 1;

    const index_t source_index = to_small_coords(big_ind);
    const function_value_t* ptr = &m(source_index);
    const auto &my_val = *ptr;

    mask_t excluded = 0;
    // Starting from 1, the voxel is treated differently
    for (size_t i = 1; i < face_index_deltas.size(); ++i) {
      if ((excluded & (one << i)) == (one << i))  // Needed for general bitsets, but slow.
        continue;

      const auto off = neighbour_address_offsest[i];
      const function_value_t &his_val = *(ptr + off);

      if (is_my_face_with_tiebreak(my_val, his_val)) {
        signature |= (one << i);
      }
      else { 
        excluded |= coface_masks[i];  // prevent checking of cofaces of i
      }
    }

    const mask_t voxel_mask = 1;
    return signature | voxel_mask;
  }

  std::vector<index_t> possible_voxels_SLOW(const index_t source,
    const index_t face_ind) {
    std::vector<index_t> result;

    const int degeneracy = 1 - source(0) % 2;
    int num_degeneracies = 0;
    for (auto coord : face_ind) num_degeneracies += ((coord % 2) == degeneracy);

    for (size_t m = 0; m < (1 << num_degeneracies); m++) {
      int curr_degeneracy = 0;
      index_t ret(0);
      for (size_t i = 0; i < face_ind.length(); i++) {
        if (face_ind[i] % 2 == degeneracy) {
          ret[i] = (m & (1 << curr_degeneracy)) ? 1 : -1;
          curr_degeneracy++;
        }
      }
      result.emplace_back(ret);
    }
    return result;
  }
};

void cubical_complex::init_faces() {
  face_index_deltas = get_voxel_face_deltas();
  // face_index_deltas.erase(face_index_deltas.begin());
  // assert(0 == std::count(face_index_deltas.begin(), face_index_deltas.end(),
  //  index_t(0)));

  index_t origin = get_base();  // we use origin+delta to make sure everything
                                // is nonnegative.
  if (origin(0) == 0) origin += 2;

  std::sort(
    face_index_deltas.begin(), face_index_deltas.end(),
    [&](const auto &a, const auto &b) { 
      return get_dim(origin + a) > get_dim(origin + b); 
  });

  for (auto delta : face_index_deltas) {
    const int id_delta =
      static_cast<int>(get_id(origin + delta)) - get_id(origin);
    const int face_type = get_cube_type(index_t(origin + delta));
    const int dim = get_dim(index_t(origin + delta));

    face_types.emplace_back(face_type);
    face_id_deltas.emplace_back(id_delta);
    face_dims.emplace_back(dim);
  }

  for (size_t i = 0; i < face_id_deltas.size(); ++i) {
    delta_to_face_number[face_id_deltas[i]] = static_cast<face_number_t>(i);
  }

  // Doesn't really matter just some voxel in the middle, so that
  // we don't get negative indices and
  // can generate all directions as shifts in linear memory.
  // Note: this is not big-to-small-index translation! 
  const index_t mid = m.extent() / 2;

  for (const index_t delta : face_index_deltas) {
    const index_t ind = mid + delta;
    ptrdiff_t off = &m(ind) - &m(mid);
    neighbour_address_offsest.emplace_back(off);
  }

  auto is_subface = [](index_t a, index_t b) -> bool {
    for (size_t i = 0; i < a.length(); i++)
      if (a[i] != 0 && a[i] != b[i]) return false;
    return true;
  };

  for (int i = 0; i < face_index_deltas.size(); i++) {
    mask_t mask = 0;
    for (int j = 0; j < face_index_deltas.size(); j++)
      if (is_subface(face_index_deltas[j], face_index_deltas[i]))
        mask |= (mask_t(1) << j);
    face_masks.push_back(mask);
  }

  for (int i = 0; i < face_index_deltas.size(); i++) {
    mask_t mask = 0;
    for (int j = 0; j < face_index_deltas.size(); j++)
      if (is_subface(face_index_deltas[i], face_index_deltas[j]))  // i,j swapped
        mask |= (mask_t(1) << j);
    coface_masks.push_back(mask);
  }
}

void cubical_complex::init_bounaries() {
  const size_t num_types = 1 << embedding_dim;
  boundary_deltas.resize(num_types);
  boundary_id_deltas.resize(num_types);
  type_to_voxel_index_offset.resize(num_types);
  type_to_voxel_address_offset.resize(num_types);

  for (size_t type_id = 0; type_id < (1ULL << embedding_dim); type_id++) {
    index_t ind(0);
    for (size_t i = 0; i < embedding_dim; i++) {
      if ((type_id & ((1ULL) << i))) {
        ind[embedding_dim - i - 1] = 1;
      }
    }

    const size_t computed_type = get_cube_type(ind);
    assert(computed_type == type_id);

    const index_t big_ind = ind;

    type_to_voxel_index_offset[type_id] =
      possible_voxels_SLOW(get_base(), big_ind);

    // Just some guy in the middle to avoid negative indices.
    // Note: this is not big-to-small-index translation! 
    const index_t mid = m.extent() / 2;
    for (const index_t delta : type_to_voxel_index_offset[type_id]) {
      const index_t ind = mid + delta;
      ptrdiff_t address_off = &m(ind) - &m(mid);
      type_to_voxel_address_offset[type_id].emplace_back(address_off);
    }

    for (size_t i = 0; i < embedding_dim; i++)
      if (ind[i] == 1) {
        index_t delta_1{ 0 };
        index_t delta_2{ 0 };
        delta_1[i] = 1;
        delta_2[i] = -1;

        boundary_deltas[type_id].emplace_back(delta_1);
        boundary_deltas[type_id].emplace_back(delta_2);

        int delta1 =
          static_cast<int>(get_id(big_ind + delta_1)) - get_id(big_ind);
        int delta2 =
          static_cast<int>(get_id(big_ind + delta_2)) - get_id(big_ind);

        boundary_id_deltas[type_id].emplace_back(delta1);
        boundary_id_deltas[type_id].emplace_back(delta2);
      }
  }
}

void cubical_complex::dump_star(const mask_t &m) {
  std::cout << "star config " << m << ": ";
  for (size_t i = 0; i < face_index_deltas.size(); i++) {
    if ((m & (mask_t(1) << i)) == (mask_t(1) << i))  // sloow for d > 3
    {
      std::cout << index_t(this->get_base() + face_index_deltas[i]) << " ";
    }
  }

  if (embedding_dim == 2) {
    bool x[3][3] = {};

    for (size_t i = 0; i < face_index_deltas.size(); i++) {
      if ((m & (mask_t(1) << i)) == (mask_t(1) << i))
      {
        index_t pos = this->get_base() + face_index_deltas[i];
        x[pos(0)][pos(1)] = true;
      }
    }

    std::cout << std::endl;

    for (int i = 0; i < 3; i++) {
      for (int j = 0; j < 3; j++) 
        std::cout << (x[i][j] ? "*" : ".");
      std::cout << std::endl;
    }
  }
  std::cout << std::endl;
}

template <typename fun_t>
void cube_handle::for_each_face_in_star(fun_t f) const {
  comp->for_each_face_in_star_of(*this, f);
}

template <typename fun_t>
void cube_handle::for_each_face(fun_t f) const {
  comp->for_each_face_of(*this, f);
}

template <typename fun_t>
void cube_handle::for_each_boundary(fun_t f) const {
  comp->for_each_boundary_of(*this, f);
}


// Offset between adjacent images/complexes.
// It's used by emitting cell.id + offset * chunk,
// And makes sure these global cell ids are consistent
// across adjacent images/complexes.
//
// It's computed from the (difference of) large coords of 
// the first cell where our data is
// to the last coord directly below it. This is not 
// the total number of cells in the complex!
// Note we substruct the cells which are in the padding
// and in the overlapping
size_t get_offset_between_adjacent_complexes(cubical_complex &c)
{
  index_t offset_point(0);
  offset_point[0] = 2 * c.m.extent(0) - 4 - 4 * overlap;
  size_t offset = c.get_id(offset_point);
  return offset;
}