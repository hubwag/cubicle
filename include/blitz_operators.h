#pragma once

namespace blitz {

bool operator<(const index_t &a, const index_t &b) {
  return std::lexicographical_compare(a.begin(), a.end(), b.begin(), b.end());
}

bool operator==(const index_t &a, const index_t &b) {
  return std::equal(a.begin(), a.end(), b.begin(), b.end());
}

template <typename T1, typename T2>
std::ostream &operator<<(std::ostream &os, const std::pair<T1, T2> &x) {
  return os << x.first << "," << x.second;
}

}
