/*  Copyright 2015-2018 IST Austria
    File contributed by: Hubert Wagner.
    This file is part of Cubicle.
    Cubicle is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
    Cubicle is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.
    You should have received a copy of the GNU Lesser General Public License
    along with Cubicle.  If not, see <http://www.gnu.org/licenses/>. */
    
#pragma once

#include "common.h"

/*
Chunk structure with padding, x == max.
The number of slices in previous and next are now 
controlled by the 'overlap' constant.
Usually we need overlap = 2, so that
the vector field touching 
the interface between images is consistent.

xxxxxxxxx
xpppppppx previous slice (or x)
xcccccccx current
xcccccccx
  ....
xcccccccx  current
xnnnnnnnx  next slice (or x)
xxxxxxxxx
*/

index_t try_get_size_from_filename(std::string s) {
  // Note the non-greedy match to avoid eating up the first number
  std::regex re(".*?(\\d+)[xX](\\d+)[xX](\\d+).*");

  std::smatch m;
  if (std::regex_search(s, m, re))
  {
    // m contains also the entire match, that we don't need.
    if (m.size() != embedding_dim + 1)
      return index_t{ 0 };

    index_t sz;
    for (int i = 0; i < embedding_dim; i++)
    {
      // m[0] is the whole match, we skip it.
      sz[i] = atoi(m[i + 1].str().c_str());
    }
    return sz;
  }
  return index_t{ 0 };
}

template <typename stream_t>
size_t get_file_size(stream_t &f) {
  const size_t old_pos = f.tellg();
  f.seekg(f.end);
  f.seekg(0, f.end);
  auto size = f.tellg();
  f.seekg(old_pos, f.beg);
  return size;
}

template <typename stream_t>
index_t try_get_file_header(stream_t &f) {
  auto nr_elems = get_file_size(f) / sizeof(file_elem_type);
  const size_t old_pos = f.tellg();

  f.seekg(0, f.beg);

  index_t ind;
  for (int i = 0; i < embedding_dim; i++)
  {
    file_elem_type v;
    f.read(reinterpret_cast<char*>(&v),
      sizeof(file_elem_type));
    ind[i] = v;
  }

  f.seekg(old_pos, f.beg);

  if (nr_elems - embedding_dim != num_voxels(ind))
  {
    return index_t{ 0 }; // failed
  }

  return ind;
}

template <typename stream_t>
size_t get_header_size_in_bytes(stream_t &f)
{
  index_t ind = try_get_file_header(f);
  if (ind == index_t{ 0 })
    return 0;
  return embedding_dim * sizeof(file_elem_type);
}

void check_file_or_exit(std::string filename) {
  std::ifstream f(filename, std::ios::binary);
  if (!f.good()) {
    std::cerr << "could not open file " << filename << std::endl;
    exit(-1);
  }
}

index_t get_input_dimensions_or_exit(const std::string filename, index_t sz)
{
  std::ifstream f(filename, std::ios::binary);

  size_t file_size = get_file_size(f);
  std::cout << "file of size: " << get_file_size(f) << " Bytes" << std::endl;

  const size_t num_elems_in_file = file_size / sizeof(file_elem_type);

  if (sz == index_t{ 0 })
  {
    sz = try_get_file_header(f);
    if (sz == index_t{ 0 }) {
      std::cout << "File Header not found or inconsistent with file length. Will need to determine dimensions differently." << std::endl;
    }
  }

  if (sz == index_t{ 0 })
    sz = try_get_size_from_filename(filename);

  if (sz == index_t{ 0 }) // still unspecified
  {
    std::cout << "dimensions of file were not specified (using -w or -s) and could not be read from file header. Quitting...";
    exit(-1);
  }

  std::cout << "\n\nusing size: " << sz << "\n" << std::endl;

  const size_t num_input_voxels = num_voxels(sz);
  const index_t large_sz = sz;

  if (num_elems_in_file * (1 << embedding_dim) >
    std::numeric_limits<cube_id_t>::max()) {
    std::cerr
      << "I think your **cube_id_t** type is too small to index all the "
      << file_size / sizeof(file_elem_type) * (1 << embedding_dim)
      << " cells... exiting";
    exit(-1);
  }

  if (num_elems_in_file < num_input_voxels) {
    std::cerr << "I think your input is too small... exiting" << std::endl;
    exit(-1);
  }

  if (num_elems_in_file > num_input_voxels) {
    std::cerr << "\n\tI think your file is too *large* but I'll assume"
      " you know what you are doing." << " expected: "
      << num_input_voxels << " but file contains: " << num_elems_in_file << std::endl << std::endl;
  }	return sz;
}

size_t get_chunk_size_or_exit(index_t input_sz, size_t *num_chunks)
{
  // Due to overlap etc. we need chunks of size at least 2!
  size_t chunk_height = std::max<size_t>( input_sz[0] / *num_chunks, 2);

  *num_chunks = (input_sz[0] + chunk_height - 1) / chunk_height;

  assert(*num_chunks * chunk_height >= input_sz[0]);
  assert((*num_chunks - 1) * chunk_height < input_sz[0]);
  assert(*num_chunks <= input_sz[0]);  

  if (input_sz[0] <= 2 && *num_chunks > 1)
  {
    std::cout << "For technical reasons it's not possible to use multiple slices/chunks for files with the first dimension <= 2. Just use a single chunk (-c 1). Exitting.";
    exit(-1);
  }

  return chunk_height;
}

matrix_t from_stream(std::string filename, index_t total_size, index_t chunk_size,
  size_t which_chunk, size_t num_chunks) {

  std::cout << "READING CHUNK" << which_chunk << std::endl;

  using input_value_t = file_elem_type;
  bool last = which_chunk + 1 == num_chunks;
  index_t slice_size = chunk_size;
  slice_size[0] = 1;

  std::ifstream stream(filename, std::ios::binary);

  size_t voxels = get_file_size(stream) / sizeof(file_elem_type);
  size_t header_size = get_header_size_in_bytes(stream);
  if (DEBUG) std::cout << "guessed header size: " << header_size << std::endl;

  const size_t voxels_per_slice = num_voxels(slice_size); // slice as in thin slice
  const size_t voxels_per_chunk = num_voxels(chunk_size);

  size_t offset_in_file = header_size + which_chunk * voxels_per_chunk * sizeof(file_elem_type);
  if (which_chunk > 0)
    offset_in_file -= overlap * voxels_per_slice * sizeof(file_elem_type);

  const size_t original_chunk_height = chunk_size[0];

  stream.seekg(offset_in_file); // move to beginning of our data   

  chunk_size += 2; // in each dimension
  chunk_size[0] += 2 * overlap;
  matrix_t M(chunk_size);

  auto mn = std::numeric_limits<matrix_t::T_type>::min();
  auto mx = std::numeric_limits<matrix_t::T_type>::max();
  M.initialize(mx);

  blitz::RectDomain<embedding_dim> data_range(M.lbound() + 1, M.ubound() - 1);

  if (which_chunk == 0) data_range.lbound()[0] += overlap;

  if (last)
  {
    data_range.ubound()[0] -= overlap;
    int adjusted_data_height = total_size[0] - which_chunk * original_chunk_height;
    assert(original_chunk_height - adjusted_data_height >= 0);
    data_range.ubound()[0] -= original_chunk_height - adjusted_data_height;
    assert(adjusted_data_height + which_chunk * original_chunk_height == total_size[0]);
  }

  matrix_t submat = M(data_range);
  size_t elems = submat.numElements();

  for (auto it = submat.begin(), end = submat.end(); it != end; ++it) {
    input_value_t v;
    stream.read(reinterpret_cast<char*>(&v),
      sizeof(input_value_t));  // Slow, but not a bottleneck.    
    *it = v;
  }

  function_value_t data_mn = *std::min_element(submat.begin(), submat.end());
  function_value_t data_mx = *std::max_element(submat.begin(), submat.end());  

  std::cout << "mn " << data_mn << " mx " << data_mx << std::endl;  

  return M;
}

void save_huge_file() {
  std::ofstream h("c:/work/huge.raw", std::ios::binary);
  std::vector<char> tmpv(2048);

  for (size_t i = 0; i < 2048ULL * 2048; i++) {
    generate_n(tmpv.begin(), tmpv.size(),
      [=]() { return (i % 100 + rand() % 2) % 100; });

    h.write(tmpv.data(), tmpv.size());
  }
}
