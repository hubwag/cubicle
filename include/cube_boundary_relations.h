/*  Copyright 2015-2018 IST Austria
    File contributed by: Hubert Wagner.
    This file is part of Cubicle.
    Cubicle is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
    Cubicle is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.
    You should have received a copy of the GNU Lesser General Public License
    along with Cubicle.  If not, see <http://www.gnu.org/licenses/>. */
    
#pragma once

int get_dim(const index_t &ind) {
  int s = 0;
  for (auto a : ind) s += (a & 1);
  return s;
}

std::vector<index_t> get_codimension_indices(const index_t &big_coords,
  int dim_difference) {
  std::vector<index_t> boundary;
  for (size_t pos = 0; pos < big_coords.length(); ++pos) {
    for (int sign = -1; sign <= 1; sign += 2) {
      index_t big_coords_adjacent = big_coords;
      big_coords_adjacent[pos] += sign;

      if (get_dim(big_coords_adjacent) - get_dim(big_coords) ==
        dim_difference) {
        boundary.emplace_back(big_coords_adjacent);
      }
    }
  }
  return boundary;
}

std::vector<index_t> get_coboundary_indices(const index_t &big_coords) {
  return get_codimension_indices(big_coords, 1);
}

std::vector<index_t> get_boundary_indices(const index_t &big_coords) {
  return get_codimension_indices(big_coords, -1);
}

std::vector<index_t> get_voxel_face_deltas() {
  std::vector<index_t> ret;
  size_t k = 2 * embedding_dim;
  for (size_t m = 0; m < (1ULL << k); m++) {
    index_t ind = { 0 };
    bool ok = true;
    for (size_t d = 0; d < embedding_dim; d++) {
      if ((m & (3ULL << (2 * d))) == (1ULL << (2 * d))) {
        ind[d] = 1;
      }
      else if ((m & (3ULL << (2 * d))) == (2ULL << (2 * d))) {
        ind[d] = -1;
      }
      else if ((m & (3ULL << (2 * d))) == (3ULL << (2 * d))) {
        ok = false;
      }
    }

    if (ok) ret.emplace_back(ind);
  }
  return ret;
}
