/*  Copyright 2015-2018 IST Austria
    File contributed by: Hubert Wagner.
    This file is part of Cubicle.
    Cubicle is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
    Cubicle is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.
    You should have received a copy of the GNU Lesser General Public License
    along with Cubicle.  If not, see <http://www.gnu.org/licenses/>. */
    
#pragma once
#include "common.h"

class image_statistics {
  std::vector<function_value_t> min_val;
  std::vector<function_value_t> max_val;
public:

  image_statistics(size_t num_voxels) : min_val(num_voxels, std::numeric_limits<function_value_t>::max()),
                                        max_val(num_voxels, std::numeric_limits<function_value_t>::min()) {
  }

  void update(function_value_t val, size_t chunk) {
    min_val[chunk] = std::min(min_val[chunk], val);
    max_val[chunk] = std::max(max_val[chunk], val);
  } 

  function_value_t get_min_value() const {
    return *std::min_element(min_val.begin(), min_val.end());
  }

  function_value_t get_max_value() const {
    return *std::max_element(max_val.begin(), max_val.end());
  }
};
