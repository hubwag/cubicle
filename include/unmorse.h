/*  Copyright 2015-2018 IST Austria, 2021-2023 University of Florida
    File contributed by: Hubert Wagner.
    This file is part of Cubicle.
    Cubicle is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
    Cubicle is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.
    You should have received a copy of the GNU Lesser General Public License
    along with Cubicle.  If not, see <http://www.gnu.org/licenses/>. */
    
#pragma once

#include <algorithm>
#include "common.h"
#include "cubical_complex.h"

template<typename get_star_mgr_t>
void dump_cubemap(cubical_complex &comp, std::vector<short> &config_number_by_voxel, 
  get_star_mgr_t get_star_manager, std::vector<bool> &is_critical, int offset, int slice) {
  if (embedding_dim != 2) {
    // std::cout << "outputting cubemap only supported in 2D!" << std::endl;
    return;
  }

  if (is_critical.size() > 10000) {
    std::cout << "outputting cubemap only supported for small 2D images" << std::endl;
    return;
  }

  blitz::Array<std::string, embedding_dim> M(comp.large_extent);

  std::string pat_by_type[] = { ".", "_", "|", "x" };

  std::stringstream ss;
  ss << "vis_slice_" << slice << ".py";
  std::ofstream pout(ss.str());

  assert(pout.good());

  // pout << "from vislib import *" << std::endl;

  float shrink = 1;  

  comp.for_each_voxel_including_interface([&](const cube_handle &v) {
    if (v.val >= INFINITE_FUNCTION_VALUE) return; // continue		  
    
    star_manager &st = get_star_manager(comp.get_index_from_id(v.id));

    auto center = comp.get_index_from_id(v.id);

    auto block_id = v.id;
    auto block_val = v.val;
    
    auto sign = st.configurations[ config_number_by_voxel[comp.get_voxel_small_id(v.id)] ];
    // auto arrows = st.matched_offsets_low_high[comp.get_star_signature(v)]; // standard mask without voxel
    auto arrows = st.matched_offsets_low_high[sign]; // standard mask without voxel

    // std::cout << v.id << " has # arrows: " << arrows.size() << std::endl;

    v.for_each_face_in_star([&](const cube_handle &f)
    {
      const auto ind = comp.get_index_from_id(f.id);
      const auto type = ::get_cube_type(ind);

      int gid = f.id + offset * slice;

      for (auto x : arrows)
      {
        if ((f.id - v.id) == x.first) {
          blitz::TinyVector<float, embedding_dim> pos1 = ind;
          pos1 = center + (pos1 - center)*shrink;
          const auto ind_high = comp.get_index_from_id(x.second + v.id);
          blitz::TinyVector<float, embedding_dim> pos2 = ind_high;
          pos2 = center + (pos2 - center)*shrink;
          pout << "draw_arrow(" 
              << pos1[1] << ", " << pos1[0] << ", " << pos2[1] << ", " << pos2[0]
              << ", value=" << block_val
              << ", block_id=" << block_id  
              <<  ")" 
              << std::endl;
        }
      }

      switch (type) {
      case(0) : {
        blitz::TinyVector<float, embedding_dim> pos = ind;

        pos = center + (pos - center)*shrink;
        pout << "draw_pt(" << pos[1] << ", " << pos[0]
          << ", value=" << block_val
          << ", block_id=" << block_id          
          << ", cell_id=" << gid      
          << ", critical=" << (is_critical[f.id] ? "True" : "False")
          << ")" << std::endl;
        break;
      }
      case(1) : case(2) : {
        std::vector<blitz::TinyVector<float, embedding_dim>> edge;

        f.for_each_boundary([&](const cube_handle &bd) {
          blitz::TinyVector<float, embedding_dim> pos = comp.get_index_from_id(bd.id);
          pos = center + (pos - center)*shrink;
          edge.emplace_back(pos);          
        });

        pout << "draw_edge(";
        for (int i = 0; i < 2; i++)
          pout << edge[i][1] << ", " << edge[i][0] << (i ? "" : ", ");

        pout << ", value=" << block_val            
            << ", block_id=" << block_id            
            << ", cell_id=" << gid
            << ", critical=" << (is_critical[f.id] ? "True" : "False")
            << ")" << std::endl;
        break;
      }
      case(3) : {
        auto val = v.val;
        // if (val < 0) val = -(val + 1000); // ...??
        pout << "draw_voxel(" << center[1] << ", " << center[0]
          << ", value=" << block_val
          << ", block_id=" << block_id          
          << ", cell_id=" << gid
          << ", critical=" << (is_critical[f.id] ? "True" : "False")
          << ")" << std::endl;
        break;
      }
      }
    });
  });

  // pout << "plt.show()";
}

// Replace with topsort+traverse approach? What's the worst case here?
template <typename fun_t>
void follow_discrete_morse_gradient(
  std::vector<cube_id_t> &col, fun_t show_me_a_hero,
  from_phat::bit_tree_column &intermediate_col,
  from_phat::bit_tree_column &critical_cells,
  int dim) {
  for (auto x : col) {
    intermediate_col.add_index(x);
  }

  col.clear();

  while (!intermediate_col.is_empty()) {
    auto x = intermediate_col.get_max_index();
    const auto &hero = show_me_a_hero(x, dim);

    if (hero.empty())  
    {
      // There was no coface matched with it, so it's critical.      
      critical_cells.add_index(x);
      intermediate_col.add_index(x);  // x was there, so it gets removed
    }
    else {
      intermediate_col.add_col(hero);
      // Now x is gone.
    }    
  }

  critical_cells.get_col_and_clear(col);
}

template<typename fun_t>
int follow_gradient_field_using_topsort(      
  phat::bit_tree_column &visited,    
  std::vector<int8_t> &indeg,
  fun_t &get_hero,
  const std::vector<cube_id_t> &col_start,
  std::vector<cube_id_t> *col_out,
  int dim)
{
  int integrated = 0;
  visited.clear();

  /* 
  In the first pass we only count the in-degree
  of each cell (restricting to the graph 
  emanating from the starting nodes).
  This information is used in the second path to visit
  the cells in topological sort order (this is essentially
  the Kahn 62' algorithm, but the graph is implicit).
  */
  std::queue<cube_id_t> Q;  

  for (auto x : col_start) {      
    Q.push(x);
    visited.set_index(x);
  }        

  while (!Q.empty()) {
    const cube_id_t c = Q.front();
    Q.pop();            
    const auto & col = get_hero(c, dim);    

    for (auto next : col)
    {
      if (next == c) continue;
      indeg[next]++;
      assert(indeg[next] <= 10);
      if (!visited.get(next)) {
        Q.push(next);        
        visited.set_index(next);
      }        
    }
  }    

  // Start the second pass.
  auto &parity = visited;
  parity.clear();
  
  for (auto x : col_start) {      
    // There could be a back-link, ie. there is another incoming arc! 
    // In this case we can't push it to the queue yet.
    if (indeg[x] == 0) {
      Q.push(x);
    }      
    // We *always* explicitly mark the unique arc/path from the 
    // co-face to this cell, even if there is a back-link!
    parity.add_index(x);      
  }  

  col_out->clear();

  while (!Q.empty()) {
    const auto c = Q.front();
    Q.pop();
    assert(indeg[c] == 0);
    integrated++;
    auto & col = get_hero(c, dim);    

    // Critical. Note that killers (arrowheads) 
    // yield empty col!
    if (col.size() == 0 && parity.get(c)) {      
        col_out->push_back(c);            
    }

    for (auto next : col)
    {
      if (next == c) continue;
      indeg[next]--;
      assert(indeg[next] >= 0);
      if (indeg[next] == 0) {
        Q.push(next);          
      }
      // If # paths is odd, switch.
      if (parity.get(c)) {
        parity.add_index(next);
      }
    }
  }  

  parity.clear();    
  return integrated;  
}

template <typename boundary_stream_t>
void compute_boundaries(matrix_t m, const star_manager &stars,
  boundary_stream_t *out_boundary_stream, size_t chunk, size_t thread) {
  Stopwatch sw;
  std::cout << "START computing ENTIRE bdry matrix for chunk " << chunk << std::endl;

  cubical_complex comp(m);
    
  size_t offset = get_offset_between_adjacent_complexes(comp);  

  comp.for_each_voxel([&](const cube_handle &v) {    
    v.for_each_face_in_star([&](const cube_handle &f) {
      // const bool is_interface = comp.is_interface_cell(f.id);            
      out_boundary_stream->put_cell(f.dim, f.id + chunk * offset, v.val, thread);      
      f.for_each_boundary([&](const cube_handle &b) {
        out_boundary_stream->put_relation(b.id + chunk * offset, f.id + chunk * offset, thread);        
      });
    });
  });

  // out_boundary_stream->finalize_stream(thread, chunk);

  std::cout << "Computed the ENTIRE bdry matrix in" << sw.lap() << std::endl;
}

size_t estimate_num_relations_in_full_matrix(const size_t num_voxels)
{  
  // Explicit formula: num_voxels * sum_i=0^D (D choose i) * 2*i
  return num_voxels * (1 << embedding_dim) * embedding_dim;
}

template <typename boundary_stream_t>
void compute_morse_boundaries(
  matrix_t &m, 
  star_manager *stars_ptr, 
  star_manager *stars_upper_fixed_ptr, 
  star_manager *stars_lower_fixed_ptr,
  boundary_stream_t *out_boundary_stream,
  bool use_twist, size_t chunk, size_t all_chunks,
  bool use_topsort, 
  size_t thread) 
{
  int slice = chunk;  

  if (DEBUG) std::cout << m << std::endl;

  star_manager &stars = *stars_ptr;
  star_manager &stars_upper_fixed = *stars_upper_fixed_ptr;
  star_manager &stars_lower_fixed = *stars_lower_fixed_ptr;

  Stopwatch st;
  from_phat::bit_tree_column intermediate_col;
  from_phat::bit_tree_column fixed_col;

  cubical_complex comp(m);
  const size_t num_voxels = comp.num_voxels();
  const size_t num_cells = comp.num_cells();

  std::cout << "size of submat" << m.extent() << " in dim " << embedding_dim
    << " with " << comp.num_voxels() / 1000000. << " M voxels and "
    << comp.num_cells() / 1000000. << " M cells" << std::endl;

  std::cout << "initialized complex " << st.lap() << std::endl;

  Stopwatch sw_no_preproc;

  const size_t offset = get_offset_between_adjacent_complexes(comp);

  std::vector<bool> is_critical(num_cells);
  std::vector<bool> is_killer(num_cells);  // destroyer of a class 
  std::vector<bool> is_killed(num_cells);  // creator of a class that is destroyed
  std::vector<bool> is_emitted(num_cells);

  std::cout << "allocated bitvectors " << st.lap() << std::endl;  

  // Large!
  std::vector<short> config_number_by_voxel(num_voxels);          // only for 2d/3d... char for 2d...
  std::vector<char> index_to_voxel_id_offset(num_cells);  // 27, 5b

  std::cout << "allocated big arrays " << st.lap() << std::endl;

  st.lap();   

  auto get_correct_star_manager = [&](index_t voxel_large_index)->star_manager& {
    auto z = (voxel_large_index[0] - 1) / 2;    
    auto extentz = comp.m.extent(0);
    bool first = chunk == 0;
    bool last = chunk == (all_chunks-1);
    if (z <= overlap - 1) // 
      return stars;
    if (z == overlap && !first)
      return stars_lower_fixed;
    if (z == overlap + 1 && !first)
      return stars_upper_fixed;

    if (z >= extentz - 1 - overlap + 1) // 
      return stars;
    if (z == extentz - 1 - overlap && !last) // 
      return stars_upper_fixed;
    if (z == extentz - 1 - overlap - 1 && !last) // 
      return stars_lower_fixed;

    return stars;
  };

  comp.init_interface_info();

  // Precompute offsets etc.
  comp.for_each_voxel_including_interface([&](const cube_handle &voxel) {
    auto sign = comp.get_star_signature(voxel);
    config_number_by_voxel[comp.get_voxel_small_id(voxel.id)] = stars.config_numbers[sign];
  });

  auto fill_criticals_etc = [&](const cube_handle &voxel) {
    star_manager &current_stars_used = get_correct_star_manager(voxel.big_ind);
    const auto sign = current_stars_used.configurations[config_number_by_voxel[comp.get_voxel_small_id(voxel.id)]];

    for (const cube_id_t off : current_stars_used.critical_offsets[sign]) {
      is_critical[off + voxel.id] = true;            
    }

    for (const cube_id_t off : current_stars_used.killers_by_config[sign]) {      
      assert(!is_critical[off + voxel.id]);
      is_killer[off + voxel.id] = true;      
    }    

    // voxel.for_each_face_in_star([&](const cube_handle &f) 
    auto record_voxel_offset = [&](const cube_handle &f) {
      if (f.id == voxel.id) return;  // 'continue'      
                                     
      // This might seem reversed, think!
      index_to_voxel_id_offset[f.id] =
        comp.delta_to_face_number[voxel.id - f.id];
    };
    comp.for_each_face_of(voxel, record_voxel_offset, ~sign);
  };

  // This is the main part where information is pre-processed!
  comp.for_each_voxel_including_interface(fill_criticals_etc);   

  std::cout << "initialized arrays " << st.lap() << std::endl;   
  
  // h.report_percents();	

  int REAL_CRITICAL = 0;

  // Emit critical cells.
  comp.for_each_voxel([&](const cube_handle &v) {
    v.for_each_face_in_star([&](const cube_handle &f) {
      const bool is_interface = comp.is_interface_cell(f.id);
      // if (is_interface) std::cerr << comp.get_index_from_id(f.id) << std::endl;
      if (is_critical[f.id]) {
        assert(is_emitted[f.id] == false);
        is_emitted[f.id] = true;
        // const bool is_interface = comp.is_interface_cell(f.id);        
        out_boundary_stream->put_cell(f.dim, f.id + offset * slice, v.val, thread);
        REAL_CRITICAL++;
      }
    });
  });

  std::cout << "emitted critical in " << st.lap() << "\n";
  std::cout << "    REAL CRITICALS IN THIS CHUNK: " << REAL_CRITICAL << std::endl;

  size_t unreduced_weight = 0;
  size_t reduced_weight = 0;

  // All these is to prevent allocation, I just fill the memory allocated once,
  // here.
  const std::vector<cube_id_t> empty;
  std::vector<cube_id_t> singleton(1);

  std::vector<cube_id_t> large;
  large.reserve(1000);

  std::vector<cube_id_t> col;
  col.reserve(1000);

  intermediate_col.init(num_cells);
  fixed_col.init(num_cells);

  // Gets a column containing cell one_to_kill. Used to get rid of
  // a regular (non-critical) cell one_to_kill when counting
  // alternating paths (in follow_discrete_morse_gradient).
  // Usually there are many choices, this function chooses
  // according to the change of basis coming from the local reductions.
  auto get_hero =
    [&](const cube_id_t one_to_kill, int dim) -> const std::vector<cube_id_t> &{
    if (is_killer[one_to_kill]) {
      singleton[0] = one_to_kill; // Hack: he will just remove it due to Z2 additions, and go on.
      return singleton;
    }

    if (is_critical[one_to_kill]) {
      return empty;
    }

    // We first identify the voxel introducing this cell...
    const auto face_nr = index_to_voxel_id_offset[one_to_kill];
    const auto voxel_id = one_to_kill + comp.face_id_deltas[face_nr];
    const auto voxel_small_id = comp.get_voxel_small_id(voxel_id);

    // ... get his precomputed signature quickly (recomputing is too
    // slow)...
    const auto sign_number =
      config_number_by_voxel[voxel_small_id];    
    const auto sign = stars.configurations[sign_number];       
    
    auto &current_stars = get_correct_star_manager(comp.get_index_from_id(voxel_id)); // it uses large id and converts
    
    // And finally access the precomputed column.
    const auto &my_col =
      current_stars.hero_by_config_and_offset_from_negative_face_id[sign_number][face_nr];
    large.clear();

    // Just translate to global coordinates.
    for (const auto off : my_col) {
      // assert(!is_killer[voxel_id + off]); // We filtered them off, supposedly.             
      if (!is_killer[voxel_id + off])
        large.emplace_back(voxel_id + off);
    }    

    return large;
  };
  
  phat::bit_tree_column visited;
  visited.init(num_cells);

  std::vector<int8_t> indeg(num_cells);
  size_t num_killed_by_twist = 0;  

  dump_cubemap(comp, config_number_by_voxel, get_correct_star_manager, is_critical, offset, slice);
  
  for (int d = embedding_dim; d > 0; d--) {
    Stopwatch sw_dim;
    int col_sum = 0;
    int integrated_cells = 0;

    size_t num_killed_in_this_dim = 0;    
    
    comp.for_each_voxel([&](const cube_handle &v) {
      const auto sign = comp.get_star_signature(v);
      const auto sign_nr = stars.config_numbers[sign];      

      assert(sign_nr >= 0);
      //const auto &external_critical =
      //  stars.critical_boundaries_by_dim[d][sign_nr];
      
      // for (const auto &off_col : external_critical) 
      v.for_each_face_in_star([&](const cube_handle &cell) {
        if (!is_critical[cell.id] || cell.dim != d)
          return; // continue

        const auto critical_id = cell.id;

        // Is it killed by twist?
        // We prevent artificial criticals from being killed off.
        if (is_killed[critical_id]) { 
          return; // continue
        }

        col.clear();
        // for (auto face_off : off_col.second) {
        cell.for_each_boundary([&](const cube_handle &bd) {
          if (!is_killer[bd.id])
          {            
            col.emplace_back(bd.id);
          }
          else {
            assert(!is_critical[bd.id]);           
          }
        });
       
        unreduced_weight += col.size();        
        
        if (use_topsort) 
          integrated_cells += follow_gradient_field_using_topsort(visited, indeg, get_hero, col, &col, d);
        else
           follow_discrete_morse_gradient(col, get_hero, intermediate_col,  fixed_col, d);
     
        reduced_weight += col.size();
        col_sum += col.size();

        // Emit boundary relations
        for (auto bd_id : col) {
          assert(is_critical[bd_id]);
          // If it's on interface (perhaps also artificial) it may be emitted by the adjacent chunk!          
          
          assert(is_emitted[bd_id] || comp.is_interface_cell(bd_id));
          assert(is_emitted[critical_id]);
          // we offset the id to have compatible numbering across slices.
          out_boundary_stream->put_relation(bd_id + offset * slice, critical_id + offset * slice, thread);                    
        }

        if (use_twist && !col.empty()) {
          cube_id_t best_id = -1;
          auto best_val = std::numeric_limits<function_value_t>::min();
          std::vector<std::pair<function_value_t, cube_id_t>> val_id;

          for (auto x : col) {
            assert(is_critical[x]);
            const auto face_nr = index_to_voxel_id_offset[x];
            const auto voxel_id = x + comp.face_id_deltas[face_nr]; // get the voxel introducing this face
            const auto val = comp.get_value_from_cube_id(voxel_id);            
            
            // This seems to be the correct tie-breaking? No need to be consistent with
            // what we do in cubical_complex?
            if (val > best_val || (val == best_val && x > best_id)) {
              best_id = x;
              best_val = val;
            }
          }

          assert(best_id != -1);
          const auto mx = best_id;

          assert(is_critical[mx]);
          assert(!is_killer[mx]);

          // BEFORE: We can't kill it if the path has a chance to continue to something with lower value.
          // This may happen if the path goes to another chunk.
          // We used to check: if (!comp.is_interface_cell(mx) || best_val == v.val)           
          // NEW: Well, actually if it continues, then the guy in the other chunk would zero this out anyway...          

          // if (!comp.is_interface_cell(mx) || best_val == v.val)
          if (!comp.is_interface_cell(mx))
          {          
            ++num_killed_by_twist;
            ++num_killed_in_this_dim;
            is_killed[mx] = true;  // Twist. Its boundary won't be generated for the next, lower, dimension.        
          }
        }
      });
    });       

    std::cout << "NEW morse in dim " << d << " done in " << sw_dim.lap() << std::endl;
    std::cout << "integrated  = " << sep(integrated_cells) << " weight " << sep(col_sum) << std::endl;
    std::cout << "#killed in this dim: " << sep(num_killed_in_this_dim) << std::endl;    
  }

  // out_boundary_stream->finalize_stream(thread, chunk);
  
  std::cout << "#killed in total: " << sep(num_killed_by_twist) << std::endl;
  std::cout << "TIME W/O PREPROC " << sw_no_preproc.lap() << std::endl;     

  const size_t num_input_voxels = comp.num_voxels_without_padding();

  std::cout << "\t\tnum input voxels: " << num_input_voxels << std::endl;
  
  const size_t original_weight = estimate_num_relations_in_full_matrix(num_input_voxels);

  std::cout << "CHUNK RED weight " << reduced_weight / 1'000'000. << " Mill instead of "
    << original_weight / 1'000'000.
    << " Mill =>  peak memory decreased by a factor of "
    << original_weight * 1.0 / reduced_weight << "\n";

  std::cout << "UNREDUCED WEIGHT:" << unreduced_weight / 1'000'000. << std::endl;
}