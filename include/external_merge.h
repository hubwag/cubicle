/*  Copyright 2015-2018 IST Austria
    File contributed by: Hubert Wagner.
    This file is part of Cubicle.
    Cubicle is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
    Cubicle is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.
    You should have received a copy of the GNU Lesser General Public License
    along with Cubicle.  If not, see <http://www.gnu.org/licenses/>. */
    
#pragma once

enum stxxl_config : size_t { BlocksPerPage = 1, PagesInCache = 1, BlockSizeInBytes = 1 << 19u };
const size_t stxxl_sort_memory_in_bytes = 128 * 1024 * 1024;

/*
 Performs an external memory sort-merge-join on two streams and updates certain 
 values in the right stream. 
 
 The key used for merge is given by left_key_getter and right_key_getter. 
 The value supplied by left_value_getter is updated in the right stream
 using the right_value_getter (for matching keys).

 Additionally, stxxl sort requires a comparator for sorting. We assume this comparator
 takes a single parameter, which is a function extracting the key.  
 The templates for this left and right comparators are supplied as template parameters to the 
 described function. The comparators are instantiated using the left and right_key_getter.

 The left keys don't have to be unique.

 This approach is significantly faster than using stxxl::map. stxxl::unordered_map is
 described as slow as of stxxl 1.4 we use.
 This approach uses much less memory than using std::map or std::unordered_map.
 */
template <
  template<typename key_t> class comp_left_t,
  template<typename key_t> class comp_right_t,
  typename left_stream_t, typename right_stream_t,
  typename left_key_getter_t, typename left_value_getter_t,
  typename right_key_getter_t, typename right_value_getter_t
>
  void join_and_update_ext_generic(
    left_stream_t &left,
    right_stream_t &right,
    left_key_getter_t get_left_key,
    left_value_getter_t get_left_value,
    right_key_getter_t get_right_key,
    right_value_getter_t get_right_value) {
  using left_t = typename left_stream_t::value_type;

  stxxl::sort(
    left.begin(), left.end(),
    comp_left_t<left_key_getter_t>{get_left_key}, stxxl_sort_memory_in_bytes);

  using right_t = typename right_stream_t::value_type;
  stxxl::sort(right.begin(), right.end(),
    comp_right_t<right_key_getter_t>{get_right_key}, stxxl_sort_memory_in_bytes);

  auto i = left.begin();
  auto j = right.begin();

  for (; i != left.end() && j < right.end();) {
    const auto ikey = get_left_key(*i);
    const auto jkey = get_right_key(*j);
    if (ikey == jkey) {
      get_right_value(*j) = get_left_value(*i);
      // In case of equality advance only the right 
      // pointer, so that left keys need not be unique.
      ++j;
    }
    else if (ikey < jkey) {
      ++i;
    }
    else {      
      ++j;
    }
  }
}

template<typename input_vector_t, typename output_vector_t>
void catenate(const std::vector<input_vector_t> &inputs, output_vector_t &out) {
  size_t total_size = 0;
  for (auto & vec : inputs)
    total_size += vec.size();
  out.reserve(total_size);

  for (auto & vec : inputs)
    std::copy_n(vec.begin(), vec.size(), std::back_inserter(out));
}


