/*  Copyright 2015-2018 IST Austria
    File contributed by: Hubert Wagner.
    This file is part of Cubicle.
    Cubicle is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
    Cubicle is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.
    You should have received a copy of the GNU Lesser General Public License
    along with Cubicle.  If not, see <http://www.gnu.org/licenses/>. */
    
#pragma once

template <typename k, typename v>
std::ostream& operator<<(std::ostream& out, const std::map<k, v>& m) {
  out << "{";
  for (auto x : m) out << " ( " << x.first << " : " << x.second << ") ";
  out << "}";
  return out;
}

template <typename k, typename v>
std::ostream& operator<<(std::ostream& out, const std::unordered_map<k, v>& m) {
  out << "#{";
  for (auto x : m) out << " ( " << x.first << " : " << x.second << ") ";
  out << "}";
  return out;
}

template <typename k, typename v>
std::ostream& operator<<(std::ostream& out, const std::pair<k, v>& p) {
  out << " ( " << p.first << " : " << p.second << ") ";
  return out;
}

template <typename t0, typename t1, typename t2>
std::ostream& operator<<(std::ostream& out, const std::tuple<t0, t1, t2>& t) {
  out << " ( " << std::get<0>(t) << " , " << std::get<1>(t) << " " << std::get<2>(t) << ") ";
  return out;
}

template <typename v>
std::ostream& operator<<(std::ostream& out, const std::set<v>& m) {
  out << "{";
  for (auto x : m) out << x << " ";
  out << "}";
  return out;
}

template <typename v>
std::ostream& operator<<(std::ostream& out, const std::vector<v>& m) {
  out << "<";
  for (auto x : m) out << x << " ";
  out << ">";
  return out;
}
