/*  Copyright 2015-2018 IST Austria, 2021-2023, University of Florida,
    File contributed by: Hubert Wagner.
    This file is part of Cubicle.
    Cubicle is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
    Cubicle is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.
    You should have received a copy of the GNU Lesser General Public License
    along with Cubicle.  If not, see <http://www.gnu.org/licenses/>. */
    
#include <cstdio>
#include <mutex>

#include <cxxopts.hpp>
#include "ctpl_stl.h" // CTPL thread pool

#include "common.h"
#include "star_configs.h"
#include "streaming.h"
#include "boundary_stream.h"
#include "euler.h"
#include "unmorse.h"
#include "image_statistics.h"

void report_matrix_size(const size_t unprocessed_matrix_weight)
{
  std::cout << "the full sparse boundary matrix would have "
    << unprocessed_matrix_weight / 1'000'000'000.
    << " Bill ONES and would take: "
    << unprocessed_matrix_weight * 8 / 1'000'000'000.
    << " GB in binary format" << std::endl;
}

void init_star_manager(index_t sz_with_collar, star_manager &stars)
{
  matrix_t fake(sz_with_collar);
  cubical_complex tmp(fake);
  stars.precompute_reduced_stars(tmp);
}

void report_matrix_size_comparison(size_t tot, const size_t unprocessed_matrix_weight)
{
  std::cout << "\n\tTOTAL WEIGHT: " << tot / 1'000'000.
    << "M vs normal: " << unprocessed_matrix_weight / 1'000'000. << "M elems"
    << " RATIO (higher is better): "
    << unprocessed_matrix_weight*1.0 / tot << std::endl << std::endl;
}

void verify_using_euler(size_t num_chunks,
  const persistence_diagram_t &p,
  std::vector<euler_pair_t> &euler_char,
  function_value_t real_minimum_value)
{  
  auto pers_euler = from_persistence_to_euler_sparse(p,real_minimum_value);
  if (pers_euler != euler_char)
    std::cerr << "WRONG EULER" << " expected:\n" << euler_char << " got:\n" << pers_euler << std::endl;
  else std::cout << "SUCCESSFULLY VERIFIED THE COMPUTATIONS USING EULER CHARACTERISTIC!" << std::endl;

  assert(pers_euler == euler_char);
}

void report_remaining_time_estimate(size_t num_chunks, int s, size_t time_so_far)
{
  int remaining_chunks = num_chunks - s - 1;
  float avg_time = time_so_far*1.0 / (s + 1);
  float remaining_time = remaining_chunks * avg_time;

  std::cout << "\tprojected remaining time: " << remaining_time << " s = " << remaining_time / 60. << " min" << std::endl << std::endl;
}

void update_image_statistics(matrix_t m, image_statistics *stats, size_t chunk) {
  cubical_complex comp(m);
  comp.for_each_voxel([=](const cube_handle &voxel) {
    stats->update(voxel.val, chunk);  
  });
}

int main(int argc, char **argv) {
  std::cerr.precision(5);
  std::cout.precision(5);

  // get uninitialized config singleton
  stxxl::config * cfg = stxxl::config::get_instance();  

  const std::string stxxl_config_path = "./.stxxl.txt";

  if (std::ifstream(stxxl_config_path))
  {
    cfg->load_config_file(stxxl_config_path);
  }
  else
  {
    // create a disk_config structure.
    const size_t disk_size_in_bytes = 50ULL * (1ULL << 30); // (1ULL << 30) == 2**30 = 1G
    stxxl::disk_config disk1("./stxxl_disk.tmp", disk_size_in_bytes, "syscall delete");
    //disk1.direct = stxxl::disk_config::DIRECT_ON; // force O_DIRECT
    //                                              // add disk to config
    cfg->add_disk(disk1);
    std::cout << "configured disk with size: " << disk_size_in_bytes/(1ULL << 30) << "GB" << std::endl;
  }

  Stopwatch sw_total;
  cxxopts::Options opts(argv[0], "(Computes persistent homology of images)");  

  opts.add_options()
    ("c,chunks", "Number of chunks, (0) defaults to roughly height/4 chunks which is good for very large files", cxxopts::value<size_t>()->default_value("0"))
    ("f,file", "Raw input file.", cxxopts::value<std::string>()->default_value("NONE"))
    ("w,width", "Width of image, assuming size w^3", cxxopts::value<size_t>()->default_value("0"))
    ("s,sizes", "Dimensions W1,W2,W3...", cxxopts::value<std::string>()->default_value("0 0 0"))
    ("p,threads", "Number of threads in thread tool. Defaults to the number of hardware thread contexts.", cxxopts::value<size_t>()->default_value("0"))
    ("x,nomorse", "Do NOT Use morse-type preprocessing; produces a large boundary matrix and reduces it.")
    ("e,euler", "Compute the Euler characteristic curve only.")    
    ("v,verify", "Verify the persistence diagram with Euler characteristic curve")    
    ("notwist", "Do NOT use the twist-type optimization. Useful only for testing/benchmarking. Will lead to slight performance loss.")    
    ("nots", "Do NOT use the topsort algorithm. Uses a heuristic algorithm with possible worse complexity, which may be faster for simple inputs.");

  try
  {
    opts.parse(argc, argv);
  }
  catch (std::exception &ex){
    std::cerr << "Error parsing options: " << ex.what() << std::endl;
    std::cerr << "HELP:\n" << opts.help() << std::endl;
  }

  const size_t w = opts["w"].as<size_t>();
  const bool morse = !opts["x"].as<bool>();
  const std::string filename = opts["f"].as<std::string>();
  const std::string sz_str = opts["s"].as<std::string>();
  const bool only_euler = opts["e"].as<bool>();
  const bool verify = opts["v"].as<bool>();
  const index_t sz_from_options = w > 0 ? index_t(w) : parse_size(sz_str);
  size_t num_chunks = opts["c"].as<size_t>();  
  bool use_twist = !opts["notwist"].as<bool>();  
  const bool use_topsort = !opts["nots"].as<bool>();
  size_t num_threads = opts["p"].as<size_t>();

  freopen("cubicle_log.txt", "wt", stdout);

  check_file_or_exit(filename);    

  if (num_threads == 0)
    num_threads = std::thread::hardware_concurrency();

  index_t input_sz = get_input_dimensions_or_exit(filename, sz_from_options);

  num_chunks = std::min<size_t>(num_chunks, input_sz[0]);

  if (num_chunks == 0)
      num_chunks = input_sz[0] / 4;  

  size_t chunk_height = get_chunk_size_or_exit(input_sz, &num_chunks);

  index_t slice_sz = input_sz;
  slice_sz[0] = chunk_height;

  std::cout << "We want to do: " << num_chunks << " slices of  " << slice_sz
    << " with " << sep(num_voxels(slice_sz)) << " voxels each" << std::endl
    << std::endl;

  index_t sz_with_collar = slice_sz + 2;
  sz_with_collar[0] +=  2 * overlap;

  const size_t num_voxels_including_collar = num_voxels(sz_with_collar);

  Stopwatch sw_stars;

  star_manager star_mgr_upper_fixed(0);  
  star_manager star_mgr_lower_fixed(2);
  star_manager star_mgr; // standard  

  if (num_threads > std::thread::hardware_concurrency())
    std::cout << "You requested " << num_threads << " but your hardware supports " << std::thread::hardware_concurrency() << " hardware threads." << std::endl;

  {
    ctpl::thread_pool tp_stars(num_threads);

    // This may write to mutual file, so we can't just run it in parallel
    init_star_manager(sz_with_collar, star_mgr);

    tp_stars.push([&](int) {init_star_manager(sz_with_collar, star_mgr_upper_fixed); });
    tp_stars.push([&](int) {init_star_manager(sz_with_collar, star_mgr_lower_fixed); });   

    tp_stars.stop(true);
  }  

  std::cout << "\nstars done in: " << sw_stars.lap() << std::endl;

  Stopwatch sw_without_preproc;

  ctpl::thread_pool tp(num_threads);

  const size_t unprocessed_matrix_weight = estimate_num_relations_in_full_matrix(num_voxels(input_sz));  
  report_matrix_size(unprocessed_matrix_weight);

  size_t time_so_far = 0;  

  // euler_merger em(num_threads);
  euler_merger_sparse em_sparse(num_threads);
  concurrent_boundary_stream output_boundary_stream(num_threads);
  image_statistics stats(num_threads);

  int done_chunks = 0;
  // const int report_each = int(sqrt(num_chunks));
  const int report_each = 1;
  using mutex_lock_guard = std::lock_guard<std::mutex>;
  std::mutex progress_report_mutex;

  std::cerr << "starting preprocessing step" << std::endl << std::endl;

  for (size_t current_chunk = 0; current_chunk < num_chunks; current_chunk++) {
    if (only_euler || verify) {
      tp.push(
        [&, current_chunk = current_chunk](int t) -> void {
        Stopwatch st_data;
        matrix_t m = from_stream(filename, input_sz, slice_sz, current_chunk, num_chunks);
        std::cout << "Read file in:" << st_data.lap() << std::endl;
        update_image_statistics(m, &stats, t);
        // compute_euler_of_slice_with_merger(m, em, nullptr, t);
        compute_euler_of_slice_with_merger(m, em_sparse, nullptr, t);
      });
    }
    
    if (morse && !only_euler) {
      concurrent_boundary_stream *bdry = &output_boundary_stream;
      star_manager *mgr = &star_mgr;
      star_manager *mgr_upper = &star_mgr_upper_fixed;
      star_manager *mgr_lower = &star_mgr_lower_fixed;      
      
      tp.push([=, &done_chunks, &progress_report_mutex](int t) -> void {
        Stopwatch st_data;
        matrix_t m = from_stream(filename, input_sz, slice_sz, current_chunk, num_chunks);
        std::cout << "Read file in:" << st_data.lap() << std::endl;
        compute_morse_boundaries(m, mgr, mgr_upper, mgr_lower, bdry, use_twist, current_chunk, num_chunks, use_topsort, t);
        auto time = st_data.lap();
        size_t rem = (num_chunks - current_chunk);
        
        {          
          mutex_lock_guard lock(progress_report_mutex);
          done_chunks++;          
          if (done_chunks % report_each == 0) {            
            std::cerr << "\r";            
            std::cerr << "Done: " << std::fixed << std::setprecision(2) << 100.00*done_chunks / num_chunks << " % " << std::flush;
          }
        }
      });
    }

    if (!morse && !only_euler) {
      concurrent_boundary_stream *bdry = &output_boundary_stream;

      tp.push([=](int t) -> void {
        Stopwatch st_data;
        matrix_t m = from_stream(filename, input_sz, slice_sz, current_chunk, num_chunks);
        std::cout << "Read file in:" << st_data.lap() << std::endl;
        compute_boundaries(m, star_mgr, bdry, current_chunk, t);      
      });
    }
  }

  tp.stop(true);  

  if (only_euler) {    
    std::cout << "\nMIN VALUE IN ALL IMAGE IS: " << stats.get_min_value() << std::endl;
    std::cout << "\nMAX VALUE IN ALL IMAGE IS: " << stats.get_max_value() << std::endl;
    // auto euler_char = em.get_merged_euler_characteristic();
    auto euler_char_sparse = em_sparse.get_merged_euler_characteristic();
    //assert(euler_char == euler_char_sparse);
    std::cout << "\tEULER took "
      << sw_without_preproc.lap() << " seconds\n" << std::endl;
    std::cout << "EULER:\n" << euler_char_sparse << std::endl;
    return 0;
  }

  const size_t total_nonzeros_in_matrix = output_boundary_stream.get_nr_relations();
  report_matrix_size_comparison(total_nonzeros_in_matrix, unprocessed_matrix_weight);

  std::cerr << std::endl;
  std::cerr << "PREPROCESSING took " << sw_without_preproc.lap() << " seconds\n" << std::endl;
  std::cout << "total boundary matrix weights: "
    << total_nonzeros_in_matrix * 8 / 1'000'000.0 << " MB " << std::endl;
  std::cerr << "Will now consolidate the boundary matrix and compute persistence" << std::endl;

  const persistence_diagram_t p = output_boundary_stream.compute_persistence(filename);  

  std::cout << "Merging on disk + persistence  took "
    << sw_without_preproc.lap() << " seconds " << std::endl;

  std::cerr << "produced " << p.size() << " persistence pairs" << std::endl;

  if (verify) {
    function_value_t real_minimum_value = stats.get_min_value();
	  // auto euler_char = em.get_merged_euler_characteristic();
	  auto euler_char = em_sparse.get_merged_euler_characteristic();
	  // assert(euler_char == euler_char_sparse);
	  std::cout << "\tEULER took "
      << sw_without_preproc.lap() << " seconds\n" << std::endl;
	  verify_using_euler(num_chunks, p, euler_char, real_minimum_value);
    std::cout << "\tEULER VERIFICATION took "
      << sw_without_preproc.lap() << " seconds\n" << std::endl;
  }
 
  std::cerr << "ALL DONE, TOTAL TIME: "
    << sw_total.lap() << " seconds " << std::endl;

  return 0;
}
