/*  Copyright 2015-2018 IST Austria
    File contributed by: Hubert Wagner.
    This file is part of Cubicle.
    Cubicle is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
    Cubicle is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.
    You should have received a copy of the GNU Lesser General Public License
    along with Cubicle.  If not, see <http://www.gnu.org/licenses/>. */
    
#pragma once

#include "matrix_reduce.h"
#include "cubical_complex.h"
#include "blitz_operators.h"

class star_manager {
protected:
  int forbidden_0_index = std::numeric_limits<int>::max();
public:
  enum : size_t { max_num_configs = 32000 };
  star_manager(int forbidden_0_index = std::numeric_limits<int>::max())
    : forbidden_0_index(forbidden_0_index) {
    critical_boundaries.resize(max_num_configs);
    hero_by_config_and_offset_from_negative_face_id.resize(max_num_configs);
    for (auto &v : critical_boundaries_by_dim)
      v.resize(max_num_configs);
    for (auto &v : critical_boundaries_by_dim_numbers)
      v.resize(max_num_configs);
  }
protected:
  std::string get_configs_filename() {
    std::stringstream ss;
    ss << "configs_" << embedding_dim << ".txt";

    return ss.str();
  }

  void write_configs(const cubical_complex &comp) {
    std::ofstream out(get_configs_filename());
    const size_t num_neighbours = ipow(3, embedding_dim);
    std::set<size_t> masks;
    for (size_t m = 0; m < (1 << (num_neighbours-1)); m++) { // all now
      size_t m2 = 1 | (m<<1); // always with the voxel
      comp.enforce_subcomplex_property(m2);
      masks.insert(m2);
    }

    std::cout << "found " << masks.size() << " star configurations"
      << std::endl;

    for (auto x : masks) out << x << ' ';
  }

  std::vector<int32_t> get_star_configs(const cubical_complex &comp) {
    std::ifstream in(get_configs_filename());
    if (!in) {
      std::cerr << "Block signature list not found. I will rewrite it now, it may take a minute but we need to do this only once." << std::endl;
      write_configs(comp);
      in.close();
      in.open(get_configs_filename());
    }
    assert(in);
    std::vector<int32_t> out;
    std::istream_iterator<int32_t> beg(in);
    std::istream_iterator<int32_t> end;
    std::copy(beg, end, std::back_inserter(out));
    return out;
  }

public:
  using bdry_mat_t = std::map<cube_id_t, std::set<cube_id_t>>;

  using small_bdry_mat_t =
    std::vector<std::pair<const cube_id_t, std::vector<cube_id_t>>>;
  using smaller_bdry_mat_t =
    std::vector<std::pair<const face_number_t, std::vector<face_number_t>>>;

  // small
  std::unordered_map<mask_t, size_t> crit_by_config;
  std::unordered_map<mask_t, std::vector<cube_id_t>> killers_by_config;
  std::unordered_map<mask_t, std::vector<cube_id_t>> critical_offsets;

  // pseudo-Morse matching arrows.
  std::unordered_map<mask_t, std::vector<std::pair<cube_id_t, cube_id_t>>> matched_offsets_low_high;

  std::vector<small_bdry_mat_t> critical_boundaries;
  std::vector<small_bdry_mat_t>
    critical_boundaries_by_dim[embedding_dim + 1];
  std::vector<smaller_bdry_mat_t>
    critical_boundaries_by_dim_numbers[embedding_dim + 1];

  std::vector<std::array<std::vector<cube_id_t>, ipow(3, embedding_dim)>>
    hero_by_config_and_offset_from_negative_face_id;

  std::vector<mask_t> configurations;
  std::unordered_map<mask_t, short>
    config_numbers;  // Maps conf -> id in the config array

  void precompute_reduced_stars(cubical_complex &comp) {
    const auto star_configs = get_star_configs(comp);

    for (size_t ii = 0; ii < star_configs.size(); ii++) {
      configurations.push_back(star_configs[ii]);
      config_numbers[star_configs[ii]] = ii;
    }

    std::cout << "read star configs: " << star_configs.size() << std::endl;

    assert(star_configs.size() > 0);

    Stopwatch st;

    size_t critical_in_all_configs = 0;    

    histogram<size_t> num_critical_histogram;    

    for (auto sign : star_configs) {
      cube_handle cube{};
      cube.big_ind = comp.get_base();
      cube.id = comp.get_id(cube);
      cube.type = get_cube_type(cube.big_ind);
      cube.dim = embedding_dim;

      bdry_mat_t mat_internal;
      bdry_mat_t mat_external;

      bdry_mat_t mat_internal_flat;
      bdry_mat_t mat_external_flat;

      std::set<cube_id_t> internal_cubes;
      std::set<cube_id_t> internal_cubes_flat;

      std::unordered_map<cube_id_t, int> dims;

      auto add_internal = [&](const cube_handle &f) {
        internal_cubes.insert(f.id - cube.id);
        dims[f.id - cube.id] = f.dim;
      };

      auto generate_matrices = [&](const cube_handle &f) {
        mat_internal[f.id - cube.id];  // empty for vertices
        mat_external[f.id - cube.id];  // empty for vertices

        comp.for_each_boundary_of(f, [&](const cube_handle &b) {
          if (internal_cubes.count(b.id - cube.id)) {
            mat_internal[f.id - cube.id].insert(b.id - cube.id);
          }
          mat_external[f.id - cube.id].insert(b.id - cube.id);
        });
      };

      comp.for_each_proper_face_of(cube, add_internal, ~sign);
      comp.for_each_proper_face_of(cube, generate_matrices, ~sign);

      // It returns the true lowest one, but if adding such a column
      // would lead to a 'vertical' matching, then we pretend the column is empty. 
      // Effectively this removes  all 'vertical' matchings.
      auto lowest_one_with_preference = [&](cube_id_t col_id, const std::set<cube_id_t> &col)
      {
        if(col.empty())
          return bad_id;        
        
        index_t our_ind = comp.get_index_from_id(cube.id + col_id); // -comp.get_index_from_id(cube.id);
        
        for (auto it = col.rbegin(); it != col.rend(); ++it)
        {
          index_t elem_ind = comp.get_index_from_id(cube.id + *it); // -comp.get_index_from_id(cube.id);

          // If we are in the forbidden part, we only allow 
          // cells with the same 0 index, otherwise everything goes.
          if ((our_ind[0] != forbidden_0_index && elem_ind[0] != forbidden_0_index) || elem_ind[0] == our_ind[0])
          // if (our_ind[0] == elem_ind[0])
            return *it;
        }

        return bad_id;
      };            

      // Apply the change of basis which reduces the internal matrix_t to the
      // external matrix_t.
      simultaneous_reduce(mat_internal, mat_external, lowest_one_with_preference);

      std::vector<cube_id_t> killers;

      for (auto col : mat_internal) {
        // if (!col.second.empty()) 
        if (lowest_one_with_preference(col.first, col.second) != bad_id)
          killers.push_back(col.first);
      }

      killers_by_config[sign] = killers;

      for (auto &id_col : mat_external)
        for (auto k : killers) id_col.second.erase(k);

      std::vector<cube_id_t> local_critical;
      
      for_each_critical(mat_internal, lowest_one_with_preference, [&](cube_id_t crit_id) {
        local_critical.push_back(crit_id);        
      });

      //////      
      
      if (false) 
      {
        std::cout << " sign repr: " << sign << std::endl;
        comp.dump_star(sign);

        char x[3][3];
        std::fill(&x[0][0], &x[0][0] + 9, '.');

        for_each_critical(mat_internal, lowest_one_with_preference, [&](cube_id_t crit_id) {
          auto ind = comp.get_index_from_id(crit_id + cube.id);
          x[ind[0]][ind[1]] = 'c';
        });

        for (int i = 0; i < 3; i++)
        {
          for (int j = 0; j < 3; j++) {
            std::cout << x[i][j];
          }
          std::cout << std::endl;
        }

        for (auto col : mat_internal)
        {
          const auto lowest = lowest_one_with_preference(col.first, col.second);
          if (lowest != bad_id)
          {
            index_t ind1 = comp.get_index_from_id(col.first + cube.id);
            index_t ind2 = comp.get_index_from_id(lowest + cube.id);
            std::cout << "matching: " << ind1 << "," << " -> " << ind2 << std::endl;
          }
        }
      }

      ////

      std::vector<std::pair<cube_id_t, cube_id_t>> matching_arrows;      

      for (auto col : mat_internal)
      {        
        if (lowest_one_with_preference(col.first, col.second) != bad_id)
        {       
          matching_arrows.emplace_back(lowest_one_with_preference(col.first, col.second), col.first);
        }
      }

      matched_offsets_low_high[sign] = matching_arrows;

      std::set<cube_id_t> critical_set(local_critical.begin(),
        local_critical.end());

      for (auto id : local_critical) {
        const auto &col_set = mat_external[id];
        std::vector<cube_id_t> col(col_set.begin(), col_set.end());

        std::vector<face_number_t> col_numbers;
        for (auto x : col) {
          col_numbers.push_back(comp.delta_to_face_number[x]);
        }

        critical_boundaries[config_numbers[sign]].emplace_back(id, col);
        critical_boundaries_by_dim[dims[id]][config_numbers[sign]].emplace_back(
          id, col);

        critical_boundaries_by_dim_numbers[dims[id]][config_numbers[sign]]
          .emplace_back(comp.delta_to_face_number[id], col_numbers);
      };

      for (auto id_col : mat_internal) {
        //if (!id_col.second.empty()) 
        if (lowest_one_with_preference(id_col.first, id_col.second) != bad_id)
        {
          auto lowest = lowest_one_with_preference(id_col.first, id_col.second);
          auto negative_face_number = comp.delta_to_face_number[-lowest];  //?
          auto &to = hero_by_config_and_offset_from_negative_face_id
            [config_numbers[sign]][negative_face_number];
          
          auto &from = mat_external[id_col.first];          
          std::copy(from.begin(), from.end(), std::back_inserter(to));
        }
      }

      const auto crit = local_critical.size();
      num_critical_histogram.add(crit);
      critical_in_all_configs += crit;

      if (crit == 7) {
        comp.dump_star(sign);
      }

      critical_offsets[sign] = local_critical;
      crit_by_config[sign] = crit;
    }

    num_critical_histogram.report_percents();    
    
    std::cout << "pre-filled critical with hashmap " << st.lap() << "\n";
    std::cout << "all configs critical : " << critical_in_all_configs << "\n";    
    std::cout << "avg nr crit over all configs: "
      << critical_in_all_configs * 1.0 / crit_by_config.size() << "\n";    
  }
};
