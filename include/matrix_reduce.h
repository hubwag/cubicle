/*  Copyright 2015-2018 IST Austria
    File contributed by: Hubert Wagner.
    This file is part of Cubicle.
    Cubicle is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
    Cubicle is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.
    You should have received a copy of the GNU Lesser General Public License
    along with Cubicle.  If not, see <http://www.gnu.org/licenses/>. */
    
#pragma once
#include <limits>
#include "common.h"

const cube_id_t bad_id = std::numeric_limits<int>::min();

template <typename col_t>
int lowest_one__(const col_t &col) {
  if (col.empty()) return bad_id;
  return *std::max_element(col.begin(), col.end());
};

template <typename target_col_t, typename source_col_t>
void add_columns(target_col_t &target, const source_col_t &source) {
  for (auto x : source)
    if (target.count(x)) {
      target.erase(x);
    }
    else {
      target.insert(x);
    }
}

template <typename mat_t, typename lowest_one_fun_t>
void simultaneous_reduce(mat_t &mat, mat_t &shadow, lowest_one_fun_t lowest_one) {

  for (int d = embedding_dim; d >= 1; d--)
  for (auto &id_col : mat) {
    const auto id = id_col.first;
    auto &col = id_col.second;

    if (2 * d != shadow[id].size()) // dim
      continue;

    while (lowest_one(id, col) != bad_id) {
      auto f = [&](auto &candidate) {
        return lowest_one(candidate.first, candidate.second) == lowest_one(id, col);
      };
      auto colliding = std::find_if(mat.begin(), mat.end(), f);
      if (colliding->first == id)
      {
        mat[lowest_one(id, col)].clear();
        shadow[lowest_one(id, col)].clear();
        break;
      }

      add_columns(col, colliding->second);
      auto colliding_shadow = shadow[colliding->first];
      add_columns(shadow[id], colliding_shadow);
    }
  }
}

/*
void test_reduction() {
  std::map<int, std::set<int>> m = { {10, {1, 2}}, {11, {0, 2}} };
  std::map<int, std::set<int>> ms = m;
  const std::map<int, std::set<int>> exp = { {10, {1, 2}}, {11, {0, 1}} };
  simultaneous_reduce(m);
  if (m != exp) {
    std::cout << "test reduction failed!";
    throw - 1;
  }
}
*/

template <typename mat_t, typename lowest_one_function_t>
size_t count_critical(mat_t &m, lowest_one_function_t lowest_one) {
  std::map<int, bool> killed;

  for (auto c : m)
    // if (!c.second.empty()) 
    if (lowest_one(c.first, c.second) != bad_id)
      killed[lowest_one(c.first, c.second)] = true;

  size_t crits = 0;
  for (auto c : m) 
    crits += (!killed[c.first] && lowest_one(c.first, c.second) == bad_id);
  return crits;
}

template <typename mat_t, typename lowest_one_function_t, typename fun_t>
void for_each_critical(const mat_t &m, lowest_one_function_t lowest_one, fun_t f) {
  std::map<int, bool> killed;

  for (const auto &c : m)
    //if (!c.second.empty()) 
    if (lowest_one(c.first, c.second) != bad_id)
      killed[lowest_one(c.first, c.second)] = true;

  for (const auto &c : m)
    if (!killed[c.first] && lowest_one(c.first, c.second) == bad_id)
      f(c.first);
}

matrix_t prepare_test_matrix() {
  index_t extent = { 3, 4 };
  matrix_t array{extent};
  array.initialize(999);

  std::vector<int> v((extent[0] - 2) * (extent[1] - 2));
  for (int i = 0; i < v.size(); i++)
    v[i] = i;  // non-injective to test star-decomposition
  std::random_shuffle(v.begin(), v.end());

  float ind = 0;
  for (int i = 1; i < extent[0] - 1; i++) {
    for (int j = 1; j < extent[1] - 1; j++) {
      array(i, j) = v[ind++];
    }
  }

  return array;
}