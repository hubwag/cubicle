"""  Copyright 2015-2018 IST Austria
    File contributed by: Teresa Heiss, Hubert Wagner
    This file is part of Chunky Euler.
    Chunky Euler is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
    Chunky Euler is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.
    You should have received a copy of the GNU Lesser General Public License
    along with Chunky Euler.  If not, see <http://www.gnu.org/licenses/>. """

import PIL.Image
import PIL.ImageFilter
import struct
import sys
from glob import glob

def save_piramid_stack(filename):
    im = PIL.Image.open(filename).convert('L')

    im = im.resize((256, 256))

    R = max(im.size)

    for i in range(0, R):
        im2 = im.filter(PIL.ImageFilter.GaussianBlur(i))
        im2.save(filename + '{:0>6}.png'.format(i))    
            

def image_stack_to_raw(filename_prefix):

    image_stack = sorted(glob(filename_prefix))

    num_pictures = len(image_stack)

    first = image_stack[0]

    image_sz = PIL.Image.open(first).size

    sz = num_pictures, image_sz[1], image_sz[0]

    print( num_pictures, first, sz )

    if num_pictures == 1:
        size_string = 'x'.join(map(str, sz[1::]))
    else:
        size_string = 'x'.join(map(str, sz))
    dim = 3

    output_filename = first + '_t_u8_' + size_string + '.from_stack.raw'
    out = open(output_filename, 'wb')

    for i, f in enumerate(image_stack):
        im = PIL.Image.open(f).convert('L')
        assert sz[1:] == (im.size[1], im.size[0])
        data = im.getdata()        

        for v in data:
            x = struct.pack('B', int(v))
            out.write(x)

        print( i )
    
    out.close()
    print( 'output succesfully written to: {}'.format(output_filename) )

if __name__ == '__main__':    
    if len(sys.argv) < 2:
        print('Converts a stack of 2D images (.png, .jpeg etc) to raw 3D image that is u8 encoded (unsigned 8-bit-integer).')
        print('The stack can consist of one single 2D image. In this case the output is a 1 x height x width raw 3D image, which is the same as a height x width raw 2D image.')
        print('Output is written with suffix _t_u8_<size>.from_stack.raw')
        print('usage: python {} "<folder>/*.<ext>"'.format(sys.argv[0]))
        print('Exitting...')
        exit(-1)
        
    image_stack_to_raw(sys.argv[1])
