"""  Copyright 2015-2018 IST Austria
    File contributed by: Teresa Heiss, Hubert Wagner
    This file is part of Chunky Euler.
    Chunky Euler is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
    Chunky Euler is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.
    You should have received a copy of the GNU Lesser General Public License
    along with Chunky Euler.  If not, see <http://www.gnu.org/licenses/>. """

import PIL.Image
import struct
import sys
from glob import glob

def raw_to_image_stack(filename, sz):
    slice_size = sz[1] * sz[2]        
    size_string = 'x'.join(map(str, sz))    
    output_filename_template = filename + size_string + '.stack_{nr:0>6}.png'

    f = open(filename, "rb")
    
    for i in range(sz[0]):
        data = f.read(slice_size)
        print( len(data) )
        image_sz = sz[2], sz[1]
        image = PIL.Image.frombytes('L', image_sz, data)
        image.save(output_filename_template.format(nr=i))        

    print( 'output succesfully written to: {} etc.'.format(output_filename_template.format(nr = 0)) )

if __name__ == '__main__':    
    if len(sys.argv) < 5:
        print('Converts a u8 (unsigned 8bit integer) encoded raw 3D image to a stack of 2D images (.png)')
        print('When the input is a raw 2D image, set input argument <size1> to 1, and the output will be one 2D png.')
        print('Output is written with suffix <size>.stack_000000.png etc.')
        print('usage: python {} <file.raw> <size1> <size2> <size3>'.format(sys.argv[0]))
        print('Exitting...')
        exit(-1)
        
    raw_to_image_stack(sys.argv[1], tuple(map(int, sys.argv[2:])))
