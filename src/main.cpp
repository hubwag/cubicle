/*  Copyright 2015-2018 IST Austria
    File contributed by: Hubert Wagner.
    This file is part of Cubicle.
    Cubicle is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
    Cubicle is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.
    You should have received a copy of the GNU Lesser General Public License
    along with Cubicle.  If not, see <http://www.gnu.org/licenses/>. */

#include <fstream>

#include <cxxopts.hpp>
#include <random>
#include <thread>
#include <future>
#include <vector>
#include <chrono>
#include <iostream>
#include <ostream>
#include <thread>
#include <queue>
#include <set>
#include <limits>
#include <algorithm>
#include <utility>
#include <tuple>
#include <array>

#define NOMINMAX

/*#include <easylogging++.h>
INITIALIZE_EASYLOGGINGPP

using med_uint_t = uint32_t;

void set_logging_config() {
  el::Configurations defaultConf;
  defaultConf.setToDefault();

  defaultConf.setGlobally(el::ConfigurationType::Format,
                          "%datetime %level %msg");
  el::Loggers::reconfigureLogger("default", defaultConf);
}*/

#include "common.h"
#include "streaming.h"

// main is here...
#include "cubes.h"
