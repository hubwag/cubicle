cmake_minimum_required (VERSION 2.8)

enable_language(CXX)
enable_language(C)

set(CMAKE_BUILD_TYPE "Release")
set(CMAKE_CXX_STANDARD 14)

project(Cubicle)

set(THREADS_PREFER_PTHREAD_FLAG ON)
find_package(Threads REQUIRED)

add_subdirectory(external_libraries/blitz_cmake_0.9)
add_subdirectory(external_libraries/stxxl)

#SET("STXXL_DIR" "external_libraries/stxxl/build")
#find_package(STXXL REQUIRED)

#message(STATUS "STXXL_CXX_FLAGS: ${STXXL_CXX_FLAGS}")
#message(STATUS "STXXL_INCLUDE_DIRS: ${STXXL_INCLUDE_DIRS}")
#message(STATUS "STXXL_LIBRARIES: ${STXXL_LIBRARIES}")

FILE(GLOB_RECURSE headers "include/*.h")

if (NOT ${STXXL_CXX_FLAGS} STREQUAL NOTFOUND)
	set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${STXXL_CXX_FLAGS}")
endif ()

include_directories(./include)
include_directories(external_libraries/blitz_cmake_0.9/include)
include_directories(external_libraries/CTPL)
include_directories(external_libraries/cxxopts)
include_directories(external_libraries/phat/include)
include_directories(${STXXL_INCLUDE_DIRS})

#SET(CONFIG_INPUT_DIM 3 CACHE STRING "Dimension of input images, default is 3.")
#add_definitions(-Dembedding_dim=${CONFIG_INPUT_DIM})

source_group("includes" FILES ${headers})

add_executable(Cubicle src/main.cpp ${headers})

target_link_libraries(Cubicle Threads::Threads)
target_link_libraries(Cubicle ${STXXL_LIBRARIES})

