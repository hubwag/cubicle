/*  Copyright 2013 IST Austria
    Contributed by: Ulrich Bauer, Michael Kerber, Jan Reininghaus

    This file is part of PHAT.

    PHAT is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    PHAT is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with PHAT.  If not, see <http://www.gnu.org/licenses/>. */

#pragma once

#include <phat/helpers/misc.h>
#include <phat/boundary_matrix.h>

namespace phat {


    class twist_reduction_with_0_removal {
    public:
      std::vector< bool > destroyer0;
      std::vector< bool > creator0;

        template< typename Representation >
        void operator () ( boundary_matrix< Representation >& boundary_matrix ) {

            const index nr_columns = boundary_matrix.get_num_cols();
            std::vector< index > lowest_one_lookup( nr_columns, -1 );

            destroyer0.resize(nr_columns, false);
            creator0.resize(nr_columns, false);

            std::vector<bool> seen_before(nr_columns, false);

            size_t pairs0 = 0;
            
            for( index cur_dim = boundary_matrix.get_max_dim(); cur_dim >= 1 ; cur_dim-- ) {
                for( index cur_col = 0; cur_col < nr_columns; cur_col++ ) {
                  if (boundary_matrix.get_dim(cur_col) != cur_dim) {
                    continue;
                  }

                  if (boundary_matrix.is_forbidden(cur_col)) {
                  //   continue;
                  }                 

                  index lowest_one = boundary_matrix.get_max_index( cur_col );                  
                  
                  /*
                  while (lowest_one != -1 && lowest_one_lookup[lowest_one] != -1) {
                      boundary_matrix.add_to( lowest_one_lookup[ lowest_one ], cur_col );
                      lowest_one = boundary_matrix.get_max_index( cur_col );
                  }
                  */

                  // Now we get rid of creators of 0-persistence.
                  std::vector<index> fixed;                  

                  while (lowest_one != -1) {
                    if (!creator0[lowest_one] || lowest_one_lookup[lowest_one] == -1) {
                      fixed.push_back(lowest_one);
                      boundary_matrix.rep._remove_max(cur_col);
                      assert(boundary_matrix.get_max_index(cur_col) < lowest_one);
                      lowest_one = boundary_matrix.get_max_index(cur_col);
                    }
                    else {                      
                      boundary_matrix.add_to(lowest_one_lookup[lowest_one], cur_col);
                      assert(boundary_matrix.get_max_index(cur_col) < lowest_one);
                      lowest_one = boundary_matrix.get_max_index(cur_col);
                    }
                  }

                  std::reverse(fixed.begin(), fixed.end());
                  boundary_matrix.rep._set_col(cur_col, fixed);  
                  
                  lowest_one = boundary_matrix.get_max_index(cur_col);                  
                  assert(lowest_one < nr_columns);
                  assert(lowest_one >= -1);

                  if( lowest_one != -1 ) {
                      lowest_one_lookup[ lowest_one ] = cur_col;
                      // We can't do twist now???
                      //if (boundary_matrix.get_val(cur_col) == boundary_matrix.get_val(lowest_one) && !boundary_matrix.is_forbidden( lowest_one ))
                      //  boundary_matrix.clear( lowest_one );
                      if (boundary_matrix.get_val(cur_col) == boundary_matrix.get_val(lowest_one)                           
                          // pairing between interface cells is fine now (it's done consistently across adjacent chunks)!
                          && (boundary_matrix.is_forbidden(lowest_one) == boundary_matrix.is_forbidden(cur_col)) 
                          // && (!boundary_matrix.is_forbidden(lowest_one) && !boundary_matrix.is_forbidden(cur_col))
                          && !seen_before[lowest_one]) {
                        destroyer0[cur_col] = true;
                        creator0[lowest_one] = true;
                        pairs0++;
                      }
                  }

                  for (index x : fixed)
                    seen_before[x] = true;
                  boundary_matrix.finalize( cur_col );
                }
            }

            std::cout << "found 0 pairs:" << pairs0 << std::endl;

            for (index cur_col = 0; cur_col < nr_columns; cur_col++) {
              if (boundary_matrix.is_forbidden(cur_col)) {
                // continue;
              }
              if (destroyer0[cur_col]) {
                boundary_matrix.clear(cur_col);
                boundary_matrix.finalize(cur_col);
                continue;
              }

              std::vector<index> col;
              boundary_matrix.get_col(cur_col, col);              

              std::vector<index> good;

              for (index ind : col) {
                // assert(!creator0[ind]);                
                if (!destroyer0[ind])
                  good.push_back(ind);
              }

              boundary_matrix.rep._set_col(cur_col, good);
              boundary_matrix.finalize(cur_col);
            }
        }
    };

class twist_reduction {
    public:
        template< typename Representation >
        void operator () ( boundary_matrix< Representation >& boundary_matrix ) {

            const index nr_columns = boundary_matrix.get_num_cols();
            std::vector< index > lowest_one_lookup( nr_columns, -1 );
            
            for( index cur_dim = boundary_matrix.get_max_dim(); cur_dim >= 1 ; cur_dim-- ) {
                for( index cur_col = 0; cur_col < nr_columns; cur_col++ ) {
                    if( boundary_matrix.get_dim( cur_col ) == cur_dim ) {
                        index lowest_one = boundary_matrix.get_max_index( cur_col );
                        while( lowest_one != -1 && lowest_one_lookup[ lowest_one ] != -1 ) {
                            boundary_matrix.add_to( lowest_one_lookup[ lowest_one ], cur_col );
                            lowest_one = boundary_matrix.get_max_index( cur_col );
                        }
                        if( lowest_one != -1 ) {
                            lowest_one_lookup[ lowest_one ] = cur_col;
                            boundary_matrix.clear( lowest_one );
                        }
                        boundary_matrix.finalize( cur_col );
                    }
                }
            }
        }
    };
}
