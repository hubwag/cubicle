# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "C:/work/blitz_cmake_0.9/test/newet.cpp" "C:/work/blitz_cmake_0.9/test/CMakeFiles/newet.dir/newet.cpp.obj"
  )
set(CMAKE_CXX_COMPILER_ID "MSVC")

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "./include"
  "/usr/local"
  "include"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "C:/work/blitz_cmake_0.9/CMakeFiles/blitz.dir/DependInfo.cmake"
  )
